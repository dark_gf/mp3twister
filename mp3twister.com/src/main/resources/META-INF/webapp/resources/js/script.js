(function($) {
	var com = com || {};
	com.dark = com.dark || {};
	com.dark.search = (function() {
		var search = {};
		
		search.init = function() {
			var $form = $(".search-box");
			
			$form.submit(function(e) {
				e.preventDefault();
				if ($(".search-box .search-input").val().length > 0) {
					this.submit();
				}
				return false;
			});
			
			$(".search-box .search-button").click(function(e) {
				$form.submit();
			});
			
			$(".search-box .search-input").keypress(function(e) {
			    if(e.which == 13) {
			    	$form.submit();
			    }
			});
			
			
			$(".show-more-songs").click(function(e) {
				e.preventDefault();
				$(".show-more-songs").hide();
				$(".more-songs").show();
			});
			
			
			//jplayer
			if ($.jPlayer) {
				$("#jquery_jplayer").jPlayer({
					ready: function (event) {
					},
					swfPath: "/resources/js",
					supplied: "mp3",
					wmode: "window",
					smoothPlayBar: true,
					keyEnabled: true,
					remainingDuration: true,
					toggleDuration: true,
					volume: 1
				});
				
				var $jpContainer = $("#main_jplayer");
				var jPlayer = $("#jquery_jplayer").data("jPlayer");
				var $currentBlock = null; 
				
				$(".link-play-song").click(function(e) {
					e.preventDefault();
					var hash = $(this).attr("hash");
					var url = $(this).attr("data");
					var $placeholder = $("." + hash);
					
					//restore play button and hide player 
					if ($currentBlock != null) {
						$jpContainer.hide();
						$(".link-play-song", $currentBlock).show();
					}
					$currentBlock = $(this).parent().parent();
					
					//moving jPlayer to new position
					$jpContainer.detach().appendTo($placeholder);
					
					//hide play link and show player
					$(this).hide();
					jPlayer.setMedia({"mp3": url});
					$jpContainer.show();
					jPlayer.play();
				});
				
				$("#jquery_jplayer").bind($.jPlayer.event.error, function(event) {
					console.log($.jPlayer.event.error);
					switch(event.jPlayer.error.type) {
						case $.jPlayer.error.URL:
							if ($currentBlock != null) {
								$jpContainer.hide();
								$(".link-play-song", $currentBlock).replaceWith('<div class="error">Sorry, but this file in no longer available.</div>');
								$(".link-play-song", $currentBlock).show();
							}
							break;
					}
				});
			}
			
		}
		
		return search;
	})();
	
	$(com.dark.search.init);
})($)