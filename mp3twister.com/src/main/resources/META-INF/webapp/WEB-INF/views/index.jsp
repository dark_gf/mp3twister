<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/resources/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

    <meta name="description" content="Free mp3 download">
    
    <title>Mp3Jungles free mp3 download</title>
    
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet"/>

    <!--[if lt IE 9]>
      <script src="<c:url value="/resources/js/html5shiv.js" />"></script>
      <script src="<c:url value="/resources/js/respond.min.js" />"></script>
    <![endif]-->
    
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.0.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>  
</head>

<body>
    <div class="container">
        <div class="logo"></div>
        <div class="row">
            <form class="search-box-big search-box" action="/search">
                <div class="col-md-11">
                    <input type="text" class="form-control search-input" id="form-name" name="q" placeholder="Type here artist, album or title of song"/>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-lg btn-warning search-button">GO!</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        
        <div class="row">
            <div class="col-md-4">
                <div class="block-top">
                    <div class="title">Top Downloads:</div>
                    <c:forEach var="trend" items="${trendsTop}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block-top">
                    <div class="title">Top Week Downloads:</div>
                    <c:forEach var="trend" items="${trendsTopWeek}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach>
                </div>
            </div>
            <div class="col-md-4">
                <div class="block-top">
                    <div class="title">Top Month Downloads:</div>
                    <c:forEach var="trend" items="${trendsTopMoth}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach>
                </div>
            </div>
        </div>
        
    </div>
    <%@include file="includes/footer.jsp" %>
</body>
</html>
