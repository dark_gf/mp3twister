<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="/resources/images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
    
    <title>${q} mp3 download for free | Mp3Jungles</title>
    
    <meta name="description" content="Download free Mp3 Online. Download ${q} Mp3 free. Listen online or download ${q}.">
    <meta name="keywords" content="download ${q} mp3, ${q}, ${q} free mp3, ${q} mp3, ${q} download">
    
    <link href="<c:url value="/resources/css/bootstrap.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/jplayer.blue.monday.css" />" rel="stylesheet">
    <link href="<c:url value="/resources/css/main.css" />" rel="stylesheet"/>

    <!--[if lt IE 9]>
      <script src="<c:url value="/resources/js/html5shiv.js" />"></script>
      <script src="<c:url value="/resources/js/respond.min.js" />"></script>
    <![endif]-->
    
    <script type="text/javascript" src="<c:url value="/resources/js/less-1.3.3.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.0.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/jquery.jplayer.min.js" />"></script>
    <script type="text/javascript" src="<c:url value="/resources/js/script.js" />"></script>

    <!-- PopAds.net Popunder Code for mp3jungles.com -->
    <script type="text/javascript">
    var _pop = _pop || [];
    _pop.push(['siteId', 427192]);
    _pop.push(['minBid', 0]);
    _pop.push(['popundersPerIP', 0]);
    _pop.push(['delayBetween', 0]);
    _pop.push(['default', false]);
    _pop.push(['defaultPerDay', 0]);
    _pop.push(['topmostLayer', false]);
    (function() {
    var pa = document.createElement('script'); pa.type = 'text/javascript'; pa.async = true;
    var s = document.getElementsByTagName('script')[0];
    pa.src = '//c1.popads.net/pop.js';
    pa.onerror = function() {
    var sa = document.createElement('script'); sa.type = 'text/javascript'; sa.async = true;
    sa.src = '//c2.popads.net/pop.js';
    s.parentNode.insertBefore(sa, s);
    };
    s.parentNode.insertBefore(pa, s);
    })();
    </script>
    <!-- PopAds.net Popunder Code End -->
</head>

<body>
<div id="main_jplayer">
	<div id="jquery_jplayer"></div>
	<div id="jp_container_1" class="jp-audio">
	  <div class="jp-type-single">
	    <div class="jp-gui jp-interface">
	      <ul class="jp-controls">
	        <li><a href="javascript:;" class="jp-play" tabindex="1">play</a></li>
	        <li><a href="javascript:;" class="jp-pause" tabindex="1">pause</a></li>
	        <li><a href="javascript:;" class="jp-stop" tabindex="1">stop</a></li>
	        <li><a href="javascript:;" class="jp-mute" tabindex="1" title="mute">mute</a></li>
	        <li><a href="javascript:;" class="jp-unmute" tabindex="1" title="unmute">unmute</a></li>
	      </ul>
	      <div class="jp-progress">
	        <div class="jp-seek-bar">
	          <div class="jp-play-bar"></div>
	        </div>
	      </div>
	      <div class="jp-volume-bar">
	        <div class="jp-volume-bar-value"></div>
	      </div>
	      <div class="jp-time-holder">
	        <div class="jp-current-time"></div>
	        <div class="jp-duration"></div>
	      </div>
	    </div>
	    <div class="jp-no-solution">
	      <span>Update Required</span>
	      To play the media you will need to either update your browser to a recent version or update your <a href="http://get.adobe.com/flashplayer/" target="_blank">Flash plugin</a>.
	    </div>
	  </div>
	</div>
</div>


    <div class="container">
        <div class="row">
            <form class="search-box-small search-box" action="/search">
                <div class="col-md-1">
                    <a href="/"><div class="small-logo"></div></a>
                </div>
                <div class="col-md-10">
                    <input type="text" class="form-control search-input" id="form-name" value="${q}" name="q" placeholder="Type here artist, album or title of song"/>
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-lg btn-warning search-button">GO!</button>
                </div>
                <div class="clearfix"></div>
            </form>
        </div>
        <div class="row">
            <div class="col-md-12">
                <%--
                <div class="adv adv728x90">
                    <!-- Begin BidVertiser code -->
                    <SCRIPT LANGUAGE="JavaScript1.1" SRC="http://bdv.bidvertiser.com/BidVertiser.dbm?pid=519075&bid=1570154" type="text/javascript"></SCRIPT>
                    <noscript><a href="http://www.bidvertiser.com/bdv/BidVertiser/bdv_publisher.dbm">make money online</a></noscript>
                    <!-- End BidVertiser code -->
                </div>
                --%>
            </div>
        </div>
        <div class="row">
          <div class="col-md-12">
				<c:choose>
				  <c:when test="${fn:length(mp3Files) > 0}">
					<h1 class="mp3-title">${q} Mp3 Download</h1>
				  </c:when>
				  <c:otherwise>
				  	<h2> Sorry, but nothing found.</h2>
				  </c:otherwise>
				</c:choose>          
          </div>
        </div>
        <div class="row">
            <div class="col-md-8 block-results">
                <c:forEach var="mp3File" items="${mp3Files}">
	                <div class="row block-song">
                        <div class="col-md-12 tag-data">
                            <c:set var="authorHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'author')}"/>
                            <c:set var="albumHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'album')}"/>   
                            <c:set var="titleHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'title')}"/>
                            ${not empty authorHighlighted ? authorHighlighted : mp3File.getAuthor()}
                            ${not empty albumHighlighted ? albumHighlighted : mp3File.getAlbum()}
                            ${not empty titleHighlighted ? titleHighlighted : mp3File.getTitle()}
                        </div>
	                
	                    <div class="clearfix"></div>
	                    
                        <div class="col-md-12 song-name">
                            <c:set var="nameHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'name')}"/>
                            ${mp3File.getShortPath()}${not empty nameHighlighted ? nameHighlighted : mp3File.getName()}
                        </div>
	                    
                        <div class="clearfix"></div>
                        
	                    <div class="col-md-2 attrs">
	                       <fmt:formatNumber var="bitrate" value="${mp3File.getBitrate() / 1000}" maxFractionDigits="0" />
	                       ${bitrate} kbps
                        </div>
	                    <div class="col-md-2 attrs">
	                       ${mp3File.getFormattedDuration()}
                       </div>
	                    <div class="col-md-8 attrs">
	                       <fmt:formatNumber var="size" value="${mp3File.getSize() / 1000000}" maxFractionDigits="2" />
	                        ${size} mb
                        </div>
	                    <div class="col-md-2 dlplay"><a href="${mp3File.getUrl()}" target="_blank" rel="nofollow">Download</a></div>
	                    <div class="col-md-10 dlplay">
	                    	<a href="javascript:;" class="link-play-song" rel="nofollow" hash="id-${mp3File.getHash()}" data="${mp3File.getUrl()}">Play</a>
	                    	<div class="placeholder id-${mp3File.getHash()}"></div>
                    	</div>
	                    <div class="clearfix"></div>
	                    <div class="col-md-12"><div class="line"></div></div>
	                </div>
				</c:forEach>
				
				<c:if test="${mp3FilesAdditional != null}">
					<a class="show-more-songs">
						show more
					</a>
					<div class="more-songs">
		                <c:forEach var="mp3File" items="${mp3FilesAdditional}">
		                    <div class="row block-song">
		                        <div class="col-md-12 tag-data">
		                            <c:set var="authorHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'author')}"/>
		                            <c:set var="albumHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'album')}"/>   
		                            <c:set var="titleHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'title')}"/>
		                            ${not empty authorHighlighted ? authorHighlighted : mp3File.getAuthor()}
		                            ${not empty albumHighlighted ? albumHighlighted : mp3File.getAlbum()}
		                            ${not empty titleHighlighted ? titleHighlighted : mp3File.getTitle()}
		                        </div>
		                    
		                        <div class="clearfix"></div>
		                        
		                        <div class="col-md-12 song-name">
		                            <c:set var="nameHighlighted" value="${highlighting.getHighlightedField(mp3File.getId(), 'name')}"/>
		                            ${mp3File.getShortPath()}${not empty nameHighlighted ? nameHighlighted : mp3File.getName()}
		                        </div>
		                        
		                        <div class="clearfix"></div>
		                        
		                        <div class="col-md-2 attrs">
		                           <fmt:formatNumber var="bitrate" value="${mp3File.getBitrate() / 1000}" maxFractionDigits="0" />
		                           ${bitrate} kbps
		                        </div>
		                        <div class="col-md-2 attrs">
		                           ${mp3File.getFormattedDuration()}
		                       </div>
		                        <div class="col-md-8 attrs">
		                           <fmt:formatNumber var="size" value="${mp3File.getSize() / 1000000}" maxFractionDigits="2" />
		                            ${size} mb
		                        </div>
		                        <div class="col-md-2 dlplay"><a href="${mp3File.getUrl()}" target="_blank" rel="nofollow">Download</a></div>
		                        <div class="col-md-10 dlplay">
		                            <a href="javascript:;" class="link-play-song" rel="nofollow" hash="id-${mp3File.getHash()}" data="${mp3File.getUrl()}">Play</a>
		                            <div class="placeholder id-${mp3File.getHash()}"></div>
		                        </div>
		                        <div class="clearfix"></div>
		                        <div class="col-md-12"><div class="line"></div></div>
		                    </div>
						</c:forEach>
					</div>				
				</c:if>
				<div class="row block-top-second related">
				    <c:forEach var="related" items="${relatedRequests}">
				        <a href="${related.key}">${related.value}</a> &nbsp&nbsp&nbsp 
				    </c:forEach>
				</div>
            </div>
            <div class="col-md-4">
                <%--
                <div class="adv adv300x250">
                    <!-- Begin BidVertiser code -->
                    <SCRIPT LANGUAGE="JavaScript1.1" SRC="http://bdv.bidvertiser.com/BidVertiser.dbm?pid=519075&bid=1570165" type="text/javascript"></SCRIPT>
                    <noscript><a href="http://www.bidvertiser.com/bdv/BidVertiser/bdv_publisher.dbm">make money online</a></noscript>
                    <!-- End BidVertiser code -->
                </div>
                --%>

                <div class="block-top-second instructions">
		            To listen ${q} just click Play
		            <br />
		            <br />
		            To download John Legend All Of Me mp3 file for free:
		            <br />
		            1. Right click on download link -&gt; Save Link As (or Save Target As)
		            <br />
		            2. Rename file to <b>${q}.mp3</b>
		            <br />
		            3. Click save
		            <br />
                </div>
            
                <div class="block-top-second">
                    <div class="title">Last songs:</div>
                    <c:forEach var="trend" items="${trendsLast}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach>                    
                </div>
                <div class="block-top-second">
                    <div class="title">Now playing:</div>
                    <c:forEach var="trend" items="${trendsNow}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach> 
                 </div>
                <div class="block-top-second">
                    <div class="title">Top songs:</div>
                    <c:forEach var="trend" items="${trendsTop}">
	                    <a href="${trend.key}" class="link">${trend.value}</a>
                    </c:forEach>
                </div>                                
                
            </div>
        </div>
        <div class="footer">
        	<a href="/">Home</a>
       	</div>
    </div>
    <%@include file="includes/footer.jsp" %>
</body>
</html>
