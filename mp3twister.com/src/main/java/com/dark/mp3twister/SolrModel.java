package com.dark.mp3twister;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.client.solrj.util.ClientUtils;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;


public class SolrModel {
    private static Logger logger = Logger.getLogger(SolrModel.class);
    
    private HttpSolrServer solrServer;
    private JdbcPooledConnectionSource pooledConnectionSource;
    
    public SolrModel(HttpSolrServer solrServer, JdbcPooledConnectionSource jdbcPooledConnectionSource) {
        this.solrServer = solrServer;
        this.pooledConnectionSource = jdbcPooledConnectionSource;
        logger.info("new instance of SolrModel created");
    }
    
    public SolrResult getByQuery(String query, int offset, int limit) {
        SolrResult result = null;
        if (query.length() == 0) {
        	query = " ";
        }
        Dao<Mp3, String> mp3Dao;
        try {
            mp3Dao = DaoManager.createDao(pooledConnectionSource, Mp3.class);
            Mp3Service mp3FileService = new Mp3Service(mp3Dao);
        
            String queryEscaped = ClientUtils.escapeQueryChars(query);
            SolrQuery solrQuery = new SolrQuery()
                .setQuery("url_path:" + queryEscaped + " OR title:" + queryEscaped + " OR album:" + queryEscaped + " OR author:" + queryEscaped + " OR all_text:" + queryEscaped)
                .setParam("q.op", "AND")
                .setStart(offset)
                .setRows(limit)
                .setParam("qf", "name^2.0 title^1.0 album^1.0 author^1.0 url_path^0.7 all_text^0.5")
                .setHighlight(true)
                .setParam("hl.fl", "name,title,album,author")
                .setHighlightSimplePre("<b>")
                .setHighlightSimplePost("</b>")
                .addField("*,score");
            try {
                QueryResponse response = solrServer.query(solrQuery);
                //[id -> [filed name -> [list of mutli value] ] ]
                SolrHighligting highlighting = new SolrHighligting(response.getHighlighting());
                logger.debug("solr query: '" + query + "' QTime: " + response.getQTime());
                SolrDocumentList solrDocumentList = response.getResults();
                logger.debug("documents count: " + solrDocumentList.size());
                List<Long> ids = new ArrayList<Long>();
                for (SolrDocument solrDocument : solrDocumentList) {
                    ids.add(Long.valueOf((String) solrDocument.getFieldValue("id")));
                }
                result = new SolrResult(response, mp3FileService.getByIds(ids), highlighting);
            } catch (SolrServerException e) {
                logger.error(e, e);
            }
        } catch (SQLException e) {
            logger.error(e, e);
        } finally {
            //TODO: do we need to close dao?
        }
        return result;
    }
    
}
