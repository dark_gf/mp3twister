package com.dark.mp3twister.web;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Trend;
import com.dark.mp3.model.TrendService;
import com.dark.mp3twister.SolrModel;
import com.dark.mp3twister.SolrResult;
import com.dark.site.Config;
import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;


@Controller
public class Home {
    private static Logger logger = Logger.getLogger(Home.class);
    
    private static Config config;
    private SolrModel solrModel = new SolrModel(config.getSolrInstance(), config.getJdbcPooledConnectionSource());
    private TrendService trendService;
    
    private Map<String, String> trendsLast;
    private Long lastGeneratedTrendsLast = 0l;
    private Map<String, String> trendsNow;
    private Long lastGeneratedTrendsNow = 0l;
    
    public Home() throws SQLException {
    	Dao<Trend, Integer> dao = null;
		dao = DaoManager.createDao(config.getJdbcPooledConnectionSource(), Trend.class);
    	trendService = new TrendService(dao);
    }
    
    public static void setConfig(Config config) {
        Home.config = config;
    }
    
    @RequestMapping("/")
    public ModelAndView home() {
    	ModelAndView mav = new ModelAndView("index");
    	mav.addObject("trendsTop", TrendService.convertToUrlTrend(trendService.getTrends(0, 10)));
    	mav.addObject("trendsTopWeek", TrendService.convertToUrlTrend(trendService.getTrends(10, 10)));
    	mav.addObject("trendsTopMoth", TrendService.convertToUrlTrend(trendService.getTrends(20, 10)));
        return mav;
    }

    @RequestMapping("/trend")
    public ModelAndView trend(@RequestParam("q") String q, HttpServletRequest request, HttpServletResponse response) {
        return search(q, request, response, true);
    }
    
    @RequestMapping("/search")
    public ModelAndView search(@RequestParam("q") String q, HttpServletRequest request, HttpServletResponse response, Boolean isTrend) {
        ModelAndView mav = new ModelAndView("search");

        SolrResult solrResult = solrModel.getByQuery(q, 0, 60);
        List<Mp3> mp3Files = solrResult.getMp3Files();

        HashSet<String> relatedRequests = new HashSet<String>();
        for (Mp3 mp3 : mp3Files) {
            String author = textToTrend(mp3.getAuthor());
            String album = textToTrend(mp3.getAlbum());
            String title = textToTrend(mp3.getTitle());
            if (author != null) {
                relatedRequests.add(author);
                if (album != null) {
                    relatedRequests.add(author + " " + album);
                }
                if (title != null) {
                    relatedRequests.add(author + " " + title);
                }
            }
            if (album != null) {
                relatedRequests.add(album);
                if (title != null) {
                    relatedRequests.add(album + " " + title);
                }
            }
            
            if (title != null) {
                relatedRequests.add(title);
            }
            if (relatedRequests.size() > 40) break;
        }
        
        List<Mp3> mp3FilesAdditional = null;
        if (mp3Files.size() > 30) {
            List<Mp3> mp3File1 = mp3Files.subList(0, 30);
            List<Mp3> mp3File2 = mp3Files.subList(30, mp3Files.size());
            mp3Files = mp3File1;
            mp3FilesAdditional = mp3File2;
        }
        mav.addObject("mp3Files", mp3Files);
        mav.addObject("mp3FilesAdditional", mp3FilesAdditional);
        mav.addObject("highlighting", solrResult.getHighligting());
        mav.addObject("q", q);
        
        generateTrendsRotation();
    	mav.addObject("trendsLast", trendsLast);
    	mav.addObject("trendsNow", trendsNow);
    	mav.addObject("trendsTop", TrendService.convertToUrlTrend(trendService.getTrends(30, 10)));
    	mav.addObject("relatedRequests", convertToUrl(relatedRequests));
    	
    	//if it is regular search 
    	if (isTrend == null || !isTrend) {
    	    //logger.debug("saving user search: " + q);
    	    //TODO:need to implement
    	}
        return mav;
    }
    
    private void generateTrendsRotation() {
    	if (System.currentTimeMillis() - lastGeneratedTrendsLast > 600000) {
    		synchronized (lastGeneratedTrendsLast) {
    			if (System.currentTimeMillis() - lastGeneratedTrendsLast > 600000) {
    				trendsLast = TrendService.convertToUrlTrend(trendService.getRandom(10));
    				lastGeneratedTrendsLast = System.currentTimeMillis();
    			}
			}
    	}
    	if (System.currentTimeMillis() - lastGeneratedTrendsNow > 10000) {
    		synchronized (lastGeneratedTrendsNow) {
    			if (System.currentTimeMillis() - lastGeneratedTrendsNow > 10000) {
    				trendsNow = TrendService.convertToUrlTrend(trendService.getRandom(10));
    				lastGeneratedTrendsNow = System.currentTimeMillis();
    			}
			}
    	}
    }
    
    @ExceptionHandler(Exception.class)
    public ModelAndView handleError(HttpServletRequest req, Exception exception) {
      logger.error("Request: " + req.getRequestURL() + " raised " + exception);
      logger.error(exception, exception);
      ModelAndView mav = new ModelAndView();
      mav.addObject("exception", exception);
      mav.addObject("url", req.getRequestURL());
      mav.setViewName("error");
      return mav;
    }    
    
    private String textToTrend(String text) {
        if (text == null) return null;
        text = StringUtils.trim(text);
        if (text.length() < 4) return null;
        if (StringUtils.containsIgnoreCase(text, "https") || StringUtils.containsIgnoreCase(text, "www") || StringUtils.containsIgnoreCase(text, ".com")
            || StringUtils.containsIgnoreCase(text, ".net") || StringUtils.containsIgnoreCase(text, ".org")) {
            return null;
        }
        
        return text;
    }
    
    private static Map<String, String> convertToUrl(HashSet<String> data) {
        Map<String, String> map = new HashMap<String, String>();
        for (String word : data) {
            map.put("/" + Utils.encode(word) + ".html", word);
        }
        return map;
    }
}