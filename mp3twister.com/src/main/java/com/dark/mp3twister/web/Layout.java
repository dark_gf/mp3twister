package com.dark.mp3twister.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.ModelAndView;

import com.dark.mp3twister.UrlRewriteFilter;

public class Layout {
    static Logger logger = Logger.getLogger(Layout.class);
    
    public static ModelAndView applyLayout(ModelAndView mav, HttpServletRequest request, HttpServletResponse response) {
        String content = "";
        try {
            mav.getView().render(mav.getModel(), request, response);
        } catch (Exception e) {
            logger.error(e);
            return mav;
        }
        return mav;
    }
}
