package com.dark.mp3twister;

import java.util.List;

import org.apache.solr.client.solrj.response.QueryResponse;

import com.dark.mp3.model.Mp3;

public class SolrResult {
    private QueryResponse solrResponse;
    private List<Mp3> mp3Files;
    private SolrHighligting highlighting;
    
    public SolrResult(QueryResponse solrResponse, List<Mp3> mp3Files, SolrHighligting highlighting) {
        super();
        this.solrResponse = solrResponse;
        this.mp3Files = mp3Files;
        this.highlighting = highlighting;
    }
    
    public QueryResponse getSolrResponse() {
        return solrResponse;
    }
 
    public List<Mp3> getMp3Files() {
        return mp3Files;
    }
    
    public SolrHighligting getHighligting() {
    	return highlighting;
    }
}
