package com.dark.mp3twister;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


public class TestInter extends HandlerInterceptorAdapter {
	  // Obtain a suitable logger.
	  private static Log logger = LogFactory.getLog(TestInter.class);

	  /**
	   * In this case intercept the request BEFORE it reaches the controller
	   */
	  @Override
	  public boolean preHandle(HttpServletRequest request,
	      HttpServletResponse response, Object handler) throws Exception {
	    try {

	      logger.info("Intercepting: " + request.getRequestURI());

	      // Do some changes to the incoming request object
	      updateRequest(request);
	      return true;
	    } catch (SystemException e) {
	      logger.info("request update failed");
	      return false;
	    }
	  }

	  /**
	   * The data added to the request would most likely come from a database
	   */
	  private void updateRequest(HttpServletRequest request) {

	    logger.info("Updating request object");
	    request.setAttribute("commonData",
	        "This string is required in every request");
	  }

	  /** This could be any exception */
	  private class SystemException extends RuntimeException {

	    private static final long serialVersionUID = 1L;
	    // Blank
	  }
}