package com.dark.mp3twister;

public class Utils {
    
    public static String formatTime(int seconds) {
        return (seconds / 60) + ":" + (seconds % 60);
    }
}
