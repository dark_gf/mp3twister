package com.dark.mp3twister;

import java.io.IOException;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class UrlRewriteFilter implements Filter {
	static Logger logger = Logger.getLogger(UrlRewriteFilter.class);
	
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;

        String requestURI = request.getRequestURI();

        if (requestURI.toLowerCase().endsWith(".html")) {
            HttpServletResponse response = (HttpServletResponse) res;
            logger.debug(req);

            logger.debug("checking banned urls " + requestURI);
            if (isBanned(requestURI)) {
                logger.debug("url is banned " + requestURI);
                response.sendRedirect("/");
            } else {
                String q = requestURI.substring(requestURI.lastIndexOf("/") + 1, requestURI.length() - 5);
                String newURI = requestURI.substring(0, requestURI.lastIndexOf("/")) + "/trend?q=" + q;

                logger.debug("Forwarding request to " + newURI);
                req.getRequestDispatcher(newURI).forward(req, res);
            }
        } else {
            chain.doFilter(req, res);
        }
    }


	public void init(FilterConfig filterConfig) throws ServletException {
	}


	public void destroy() {
	}

	private String[] bannedArray = {};
	
	private List<String> banned = Arrays.asList(bannedArray);
	
    private boolean isBanned(String url) {
        return banned.contains(url);
    }


}