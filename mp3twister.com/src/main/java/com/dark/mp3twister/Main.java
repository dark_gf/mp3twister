package com.dark.mp3twister;

import com.dark.mp3twister.web.Home;
import com.dark.site.Config;


public class Main {
	private WebServer server;
    
	public Main() {
	    Config config = new Config();
	    Home.setConfig(config);
        server = new WebServer(config.getIntProp("site.port"));
    }
    
	public static void main(String[] args) throws Exception {
		(new Main()).start();
	}
	
    public void start() throws Exception {
        server.start();
        server.join();
    }
}
