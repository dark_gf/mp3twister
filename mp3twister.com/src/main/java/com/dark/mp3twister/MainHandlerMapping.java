package com.dark.mp3twister;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;
import org.springframework.web.servlet.handler.SimpleUrlHandlerMapping;


public class MainHandlerMapping extends SimpleUrlHandlerMapping {
	static Logger logger = Logger.getLogger(MainHandlerMapping.class);
	
	@Override
	protected Object getHandlerInternal(HttpServletRequest request) throws Exception {
		logger.info("MainHandlerMapping.getHandlerInternal()");
//		Object result = super.getHandlerInternal(request);
//		logger.info("----------------");
//		logger.info(result.getClass());
//		logger.info(result);
//		System.out.println(result);
//		logger.info("----------------");
		return "HomeController";
	}
	
	@Override
	protected void registerHandler(String urlPath, Object handler) throws BeansException, IllegalStateException {
		logger.info("MainHandlerMapping.registerHandler()");
		super.registerHandler(urlPath, handler);
	}
}
