package com.dark.mp3twister;

import java.util.List;
import java.util.Map;


public class SolrHighligting {
	private Map<String, Map<String, List<String>>> highlighting;
	
	//[id -> [filed name -> [list of mutli value] ] ]
	public SolrHighligting(Map<String, Map<String, List<String>>> highlighting) {
		this.highlighting = highlighting;
	}
	
	public String getHighlightedField(Long id, String field) {
		Map<String, List<String>> fields = highlighting.get(id.toString());
		if (fields != null) {
			List<String> values = fields.get(field);
			if (values != null && values.size() > 0) {
				return values.iterator().next();
			}
		}
		return null;
	}

}
