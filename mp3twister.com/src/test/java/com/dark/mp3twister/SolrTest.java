package com.dark.mp3twister;

import java.io.IOException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.junit.Test;
import org.junit.runner.RunWith;

import junit.framework.TestCase;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration; 
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:web-context.xml"})
public class SolrTest extends TestCase {
    static Logger logger = Logger.getLogger(SolrTest.class);
    
    @Autowired
    @Qualifier("solrServer")
    HttpSolrServer solrServer;

    
    @Test
    public void simpleQuery() throws SolrServerException, IOException {
        BasicConfigurator.configure();
        assertNotNull(solrServer);
        assertNotNull(solrServer.ping());
    }
}
