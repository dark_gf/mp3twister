package com.dark.mp3.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class Mp3FileTest {

    @Test
    public void test() {
        Mp3 mp3 = new Mp3();
        mp3.setUrl("http://www.somedomain.com/dfgdfgdfg/fsdf.mp3");
        assertEquals("www.somedomain.com/.../", mp3.getShortPath());
    }

}
