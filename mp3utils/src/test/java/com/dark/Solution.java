package com.dark;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Solution {
    
    private static List<String> getNextStep(List<String> data) {
        List<String> result = new ArrayList<String>();
        
        int dataLen = data.size();
        for (int i = 0; i < dataLen; i++) {
            result.add("0" + data.get(i));
        }
        
        for (int i = dataLen - 1; i >= 0; i--) {
            result.add("1" + data.get(i));
        }
        return result;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();
        
        if (N > 1) {
            List<String> result = new ArrayList<String>();
            result.add("0");
            result.add("1");
            for (int i = 0; i < N - 2; i++) {
                result = getNextStep(result);
                if (result.size() > N) {
                    result = result.subList(0, N);
                }
            }
            
            for (int i = N - 1; i >= 0; i--) {
                System.out.println("1" + result.get(i));
            }
        } else {
            System.out.println("1");
        }
    }
}