package com.dark.http;

import static org.junit.Assert.assertNotNull;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.dark.http.SimpleHttpClient.RequestResponce;


public class SimpleHttpClientTest {
	@Test
	public void testLimit() {
		SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
		IOException ex = null;
		try {
			RequestResponce rr = simpleHttpClient.doGetRequest("http://balkanchallenges.com/ttaram/raportet.php");
			String data = SimpleHttpClient.httpResponseToString(rr);
		} catch (IOException e) {
			ex = e;
		}
		assertNotNull(ex);
	}
	
	@Test
	public void testSimple() throws ClientProtocolException, IOException {
		SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
		RequestResponce rr;
		rr = simpleHttpClient.doGetRequest("http://fetofoto.com");
		String data = SimpleHttpClient.httpResponseToString(rr);
	}

	
}
