package com.dark.site;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;


public class Config {
    static Logger logger = Logger.getLogger(Config.class);
    
    private final String defaultEnv = "dev";
    private final String defaultPath = "../";
    
    private static Properties cacheProp;
    
    String env;
    
    public Config() {
        this.env = System.getProperty("env", defaultEnv);
    }
    
    private Properties loadProperties() {
        if (cacheProp == null) {
        	String configFullPath = defaultPath + "config-" + env + ".properties";
        	logger.info("loading config from " + configFullPath);
            try {
                Properties prop = new Properties();
                InputStream input = new FileInputStream(configFullPath);
                prop.load(input);
                cacheProp = prop;
            } catch (FileNotFoundException e) {
                logger.error(e, e);
            } catch (IOException e) {
                logger.error(e, e);
            }
        }
        return cacheProp;
    }
    
    public HttpSolrServer getSolrInstance() {
        return new HttpSolrServer(loadProperties().getProperty("solr.host"));
    }
    
    public JdbcPooledConnectionSource getJdbcPooledConnectionSource() {
        Properties prop = loadProperties();
        JdbcPooledConnectionSource pooledConnectionSource = null;
        try {
            pooledConnectionSource = new JdbcPooledConnectionSource(prop.getProperty("database.url"), prop.getProperty("database.user"), prop.getProperty("database.password"));
        } catch (SQLException e) {
            logger.error(e, e);
        }
        return pooledConnectionSource;
    }
    
    public int getMp3SearchThreadsCount() {
    	Properties prop = loadProperties();
    	return Integer.valueOf(prop.getProperty("mp3search.threadsCount"));
    }
    
    public String getStringProp(String key) {
        Properties prop = loadProperties();
        String value = prop.getProperty(key);
        if (value == null) {
            logger.error("property " + key + " not found");
        }
        return value;
    }
     
    public Integer getIntProp(String key) {
        String value = getStringProp(key);
        if (value != null) {
            return Integer.valueOf(value);
        }
        return null;
    }
}
