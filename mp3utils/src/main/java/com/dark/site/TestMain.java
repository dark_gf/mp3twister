package com.dark.site;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

import org.apache.log4j.Logger;

public class TestMain {
    private static Logger logger = Logger.getLogger(TestMain.class);

    private static Integer getMaxPosition(Long value, List<Long> data) {
        Integer dataSize = data.size();
        Integer startSegment = 0;
        Integer endSegment = dataSize - 1;
        while (startSegment < endSegment) {
            Integer tryPos = (endSegment - startSegment)/2 + startSegment;
            if (data.get(tryPos) < value) {
                startSegment = tryPos + 1;
            } else {
                endSegment = tryPos;
            }
            if (startSegment.equals(endSegment)) if (data.get(startSegment) > value) {
                startSegment--;
                endSegment--;
            }
        }
        
        return startSegment;
    }
    
    public static void main(String[] args) throws IOException {
        File file = new File("c:/Users/Yauheni_Fiatsisau/Downloads/1/test2.txt");
        BufferedReader br = new BufferedReader(new FileReader(file));
        
        List<Long> data = new ArrayList<Long>();
        String line;
        while ((line = br.readLine()) != null) {
            data.add(Long.valueOf(line));
        }
        br.close();
        
        logger.debug("done reading file");
        
        Collections.sort(data);
        
        logger.debug("done sorting");
        
//        Integer pos = getMaxPosition(27L, data);
//        logger.debug(pos);
//        logger.debug(data.get(pos));
        
        HashSet<Integer> result = new HashSet<Integer>();
        int curPos = 0;
        int dataSize = data.size();
        while (curPos < dataSize) {
            Long curNumber = data.get(curPos);
            Long minOpositeNumber = -10000 - curNumber;
            Long maxOpositeNumber = 10000 - curNumber;
            
            Integer pos = getMaxPosition(maxOpositeNumber, data);
            while (pos >= 0 && data.get(pos) >= minOpositeNumber) {
                Long curTempNumber = data.get(pos);
                if (!curTempNumber.equals(curNumber)) {
                    Integer res = (int) (curTempNumber + curNumber);
                    result.add(res);
                }
                pos--;
            }
            curPos++;
        }
        
        logger.info(result.size());

    }

}
