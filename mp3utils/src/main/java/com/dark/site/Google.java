package com.dark.site;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Google {
	public static int RESULT_OK = 0;
	public static int RESULT_CAPTCHA = 1;
	public static int RESULT_OTHER_ERROR = 2;
	
	protected int result = RESULT_OK;
	protected List<String> links = new ArrayList<String>(); 
	protected Boolean hasNextPage = false;
	
	protected Pattern[] regexLinks = {
		Pattern.compile("<h3 class=[\"']?r[\"']?><a href=\\\"(http[^\"]+)\\\""),
		Pattern.compile("<a href=\"(http[^\"]+)\" class=\"p\">"),
		Pattern.compile("\\/url\\?q=(http[^\"]+)&amp;sa=U"),
	};


	public Google(String html) {
		if (html.indexOf("To continue, please type the characters below") >= 0 && html.indexOf("client does not have permission") >= 0) {
			result = RESULT_CAPTCHA;
		} else if (html.indexOf("- did not match any documents.") >= 0) {
			//no links on page
		} else {
			for (Pattern pattern : regexLinks) {
				Matcher matcher = pattern.matcher(html);
				links = new ArrayList<String>();
				while (matcher.find()) {
				    if (!matcher.group().toLowerCase().contains("webcache.googleusercontent")) if (!links.contains(matcher.group(1))) {
				        links.add(matcher.group(1));
				    }
				}
				
			}
			if (links.size() == 0) {
				result = RESULT_OTHER_ERROR;
			}
            if (html.indexOf(">Next</span></a>") >= 0 || html.indexOf("<strong>Next</strong></a>") >= 0) {
            	hasNextPage = true;
            }			
		}
	}
	
	public Boolean hasNextPage() {
		return hasNextPage;
	}

	public int getResult() {
		return result;
	}

	public List<String> getLinks() {
		return links;
	}

	
	
//	public List<String> getLinks() {
//		hasNextPage = false;
//		//String url = "http://" + googleHost + "/search?q=" + Site.encode(query) + "&num=100&hl=en&prmd=imvnsu&start=" + Integer.toString(offset) + "&sa=N";
////		if (additionalParams.length() > 0) {
////			url = url + "&" + additionalParams;
////		}
//		String html = null;
//		for (int i = 0; i < autoRepeatRequestNumber; i++) {
//			Site site = SiteFactory.createSite(true, useProxy);
//			html = site.doGetRequest(url);
//			if (html != null) {
//				if (html.indexOf("To continue, please type the characters below") == 0 && html.indexOf("client does not have permission") == 0) break;
//			}
//		}
//		if (html == null) {
//			lastError = "Request error " + url;
//			return null;
//		} else if (html.indexOf("- did not match any documents.") > 0) {
//			return new ArrayList<String>();
//		} else if (html.indexOf("To continue, please type the characters below") > 0 || html.indexOf("client does not have permission") > 0) {
//			lastError = "Banned by google " + url;
//			return null;
//		} else {
//			for (Pattern pattern : regexLinks) {
//				Matcher matcher = pattern.matcher(html);
//				links = new ArrayList<String>();
//				while (matcher.find()) {
//					links.add(matcher.group(1));
//				}
//			}
//			if (links.size() == 0) {
//				lastError = "Other error " + url;
//				return null;
//			}
//            if (html.indexOf(">Next</span></a>") >= 0 || html.indexOf("<strong>Next</strong></a>") >= 0) {
//            	hasNextPage = true;
//            }
//			
//		}
//		return links;
//	}
	
}
