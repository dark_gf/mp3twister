package com.dark.mp3Utils;

import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;


public class ProxySupplier implements Runnable {
    private static boolean shutdown;
    private int limitStoreNumProxies = 50;
    private static ConcurrentLinkedQueue<HttpHost> proxies = new ConcurrentLinkedQueue<HttpHost>();
    static Logger logger = Logger.getLogger(ProxySupplier.class);
	
	public void run() {
	    logger.info("proxy supplier started");
	    shutdown = false;
		while (true) {
			if (shutdown) break;
			if (proxies.size() < limitStoreNumProxies) {
				paidProxylist();
			    nordvpn();
			    proxyList();
			} else {
				try {Thread.sleep(1000);} catch (InterruptedException e) {}
			}
		}
	}
	
	//don't provide proxies
	private void hidemyass() {
        for (int i = 1; i <= 10; i++) {
            SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
            RequestResponce response;
            try {
                response = simpleHttpClient.doGetRequest("http://www.hidemyass.com/proxy-list/" + i);
                String html = SimpleHttpClient.httpResponseToString(response);
                Document doc = Jsoup.parse(html);
                doc.select("script, .hidden, span[style*=display:none], div[style*=display:none]").remove();
                Element table = doc.select("#listtable > tbody").first();
                if (table == null) break;
                Elements trs = table.getElementsByTag("tr");
                for (Element tr : trs) {
                    Elements tds = tr.getElementsByTag("td");
                    String style = tds.select("style").first().html();
                    Pattern p = Pattern.compile("\\.(.*?)\\{display:none");
                    Matcher m = p.matcher(style);
                    while (m.find()) {
                        tds.select("." + m.group(1)).remove();
                    }
                    String host = tds.get(1).text().trim();
                    int port = Integer.valueOf(tds.get(2).text().trim());
                    proxies.add(new HttpHost(host, port));
                }
            } catch (ClientProtocolException e) {
                logger.warn(e, e);
            } catch (IOException e) {
                logger.warn(e, e);
            }
        }	    
	}
	
	private void freeproxylist() {
	    SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        RequestResponce response;
        try {
            response = simpleHttpClient.doGetRequest("http://free-proxy-list.net/");
            String html = SimpleHttpClient.httpResponseToString(response);
            //<td>186.89.5.68</td><td>8080</td>
            Pattern p = Pattern.compile("<td>(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})</td><td>(\\d{2,6})<");
            Matcher m = p.matcher(html);
            while (m.find()) {
                proxies.add(new HttpHost(m.group(1), Integer.valueOf(m.group(2))));
            }
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
	}
	
	//http://hideme.ru/
	//ban bots
    private void hideme() {
        SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        RequestResponce response;
        try {
            Integer[] ports = {80, 8080, 3128, 8081};
            for (Integer port : ports) {
                response = simpleHttpClient.doGetRequest("http://hideme.ru/proxy-list/?ports=" + port);
                String html = SimpleHttpClient.httpResponseToString(response);
                Pattern p = Pattern.compile(">(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})<");
                Matcher m = p.matcher(html);
                while (m.find()) {
                    proxies.add(new HttpHost(m.group(1), port));
                }
            }
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
	}
    
    
    //http://proxy-list.org/    
    private void proxyList() {
        SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        RequestResponce response;
        try {
        	int count = 0;
            for (int page = 1; page < 11; page++) {
                response = simpleHttpClient.doGetRequest("http://proxy-list.org/russian/index.php?p=" + page);
                String html = SimpleHttpClient.httpResponseToString(response);
                Matcher m = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})\\:(\\d{2,6})").matcher(html);
                while (m.find()) {
                    proxies.add(new HttpHost(m.group(1), Integer.valueOf(m.group(2))));
                    count++;
                }
            }
            logger.debug("From proxy-list.org got " + count + " proxies");
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
    }
    

    
    private void nordvpn() {
        SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        RequestResponce response;
        try {
        	int count = 0;
            for (int page = 1; page < 41; page++) {
                response = simpleHttpClient.doGetRequest("https://nordvpn.com/free-proxy-list/" + page + "/");
                String html = SimpleHttpClient.httpResponseToString(response);
                //<td>92.255.169.124</td><td>3128</td>
                Pattern p = Pattern.compile("<th>(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3})</th>[^<]+<th>(\\d{2,6})<");
                Matcher m = p.matcher(html);
                while (m.find()) {
                    proxies.add(new HttpHost(m.group(1), Integer.valueOf(m.group(2))));
                    count++;
                }
            }
            logger.debug("From nordvpn got " + count + " proxies");
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
    }
    
    private void paidProxylist() {
        SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        RequestResponce response;
        try {
        	int count = 0;
            response = simpleHttpClient.doGetRequest("http://proxy-list.org/russian/yourproxylists/limit-5000/8f0c84bd96cf020829c2d80ca6a18191.txt");
            String html = SimpleHttpClient.httpResponseToString(response);
            Pattern p = Pattern.compile("(\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}):(\\d{2,6})");
            Matcher m = p.matcher(html);
            while (m.find()) {
                proxies.add(new HttpHost(m.group(1), Integer.valueOf(m.group(2))));
                count++;
            }
            logger.debug("From paidProxylist got " + count + " proxies");
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
    }
    
    
    
    
	public static void shutdown() {
	    shutdown = true;
	}
	
	public static HttpHost get() {
	    HttpHost httpHost = null;
	    while (httpHost == null) {
    	    httpHost = proxies.poll();
    	    if (httpHost == null) {
    	        logger.warn("proxies queue is empty");
    	    }
    	    try {Thread.sleep(1000);} catch (InterruptedException e) {}
	    }
	    return httpHost;
	}
}
