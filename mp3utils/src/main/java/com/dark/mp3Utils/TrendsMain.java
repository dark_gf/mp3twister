package com.dark.mp3Utils;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;
import com.dark.mp3.model.Trend;
import com.dark.mp3.model.TrendService;
import com.dark.mp3Utils.ITunes.Callback;
import com.dark.site.Config;
import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

public class TrendsMain {
    static Logger logger = Logger.getLogger(TrendsMain.class);

    /**
     * @param args
     * @throws SQLException
     */
    public static void main(String[] args) throws SQLException {
        Config config = new Config();
        Dao<Trend, Integer> dao = DaoManager.createDao(config.getJdbcPooledConnectionSource(), Trend.class);
        final TrendService trendService = new TrendService(dao);
        final String checkUrl = config.getStringProp("site.trends.checkUrl");
        Connection connection = null;
        try {
            connection = DriverManager.getConnection(config.getStringProp("database.url") + "?user=" + config.getStringProp("database.user") + "&password="
                    + config.getStringProp("database.password"));
            connection.setAutoCommit(false);
            PreparedStatement stat = connection.prepareStatement("SELECT * FROM `words_eng` ORDER BY `id`", ResultSet.TYPE_FORWARD_ONLY,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet results = stat.executeQuery();

            while (results.next()) {
                String query = results.getString("text");
                try {
                    SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
                    RequestResponce requestResponce = simpleHttpClient.doGetRequest(checkUrl + Utils.encode(query));
                    Document doc = Jsoup.parse(SimpleHttpClient.httpResponseToString(requestResponce));
                    if (doc.select(".block-song").size() > 2) {
                        logger.info(query);
                        trendService.insertTrend(query);
                    }
                } catch (ClientProtocolException e) {
                    logger.warn(e, e);
                } catch (IOException e) {
                    logger.warn(e, e);
                }

            }

        } catch (SQLException e) {
            logger.error(e, e);
        } finally {
            if (connection != null)
                try {
                    connection.close();
                } catch (SQLException e) {
                    logger.error(e, e);
                }
        }

        // Callback cb = new Callback() {
        // public void result(String author, String title) {
        // String trend = author + " " + title;
        // SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        // try {
        // RequestResponce requestResponce =
        // simpleHttpClient.doGetRequest(checkUrl + Utils.encode(trend));
        // Document doc =
        // Jsoup.parse(SimpleHttpClient.httpResponseToString(requestResponce));
        // if (doc.select(".block-song").size() > 2) {
        // logger.info(trend);
        // trendService.insertTrend(trend);
        // trendService.insertTrend(author);
        // trendService.insertTrend(title);
        // }
        // } catch (ClientProtocolException e) {
        // logger.warn(e, e);
        // } catch (IOException e) {
        // logger.warn(e, e);
        // }
        // }
        // };
        //
        // for (int i = 0; i < 5; i++) {
        // (new Thread(new ITunes(cb))).start();
        // }

        // Billboard billboard = new Billboard();
        // billboard.getSongs(new Callback() {
        // public void result(String author, String title) {
        // String trend = author + " " + title;
        // // SimpleHttpClient simpleHttpClient = new SimpleHttpClient();
        // // try {
        // // RequestResponce requestResponce =
        // simpleHttpClient.doGetRequest(checkUrl + Utils.encode(trend));
        // // Document doc =
        // Jsoup.parse(SimpleHttpClient.httpResponseToString(requestResponce));
        // // if (doc.select(".block-song").size() > 2) {
        // logger.info(trend);
        // trendService.insertTrend(trend);
        // // }
        // // } catch (ClientProtocolException e) {
        // // logger.warn(e, e);
        // // } catch (IOException e) {
        // // logger.warn(e, e);
        // // }
        // }
        // });
        //
        trendService.shuffle();
    }

}
