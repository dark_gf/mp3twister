package com.dark.mp3Utils;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;


public class Mp3va {
    private static Logger logger = Logger.getLogger(Mp3va.class);
    
	private Document document;
	
	public Mp3va(String html, String url) {
		document = Jsoup.parse(html, url);
	}
	
    //author name -> url
	public Map<String, String> getAuthors() {
	    HashMap<String, String> result = new HashMap<String, String>();
	    Elements els = document.select(".i-view a");
	    for (Element el : els) {
	        result.put(el.text(), el.attr("abs:href"));
	    }
	    return result;
	}
	
    //album name -> url
	public HashMap<String, String> getAlbums() {
	    HashMap<String, String> result = new HashMap<String, String>();
	    Elements els = document.select(".txt h3 a");
        for (Element el : els) {
            result.put(el.text(), el.attr("abs:href"));
        }
        return result;	    
	}
	
	//song name -> duration in seconds
	public HashMap<String, Long> getSongs() {
        HashMap<String, Long> result = new HashMap<String, Long>();
        Elements trEls = document.select(".tbl-songs tr[id]");
        for (Element trEl : trEls) {
            String name = trEl.select(".td-txt").text();
            String durtaionStr = trEl.select(".td-time").text();
            String[] splited = durtaionStr.split(":");
            Long duration = 0l;
            if (splited.length == 2) {
                duration = Long.parseLong(splited[0]) * 60 + Long.parseLong(splited[1]);
            } else {
                logger.error(durtaionStr + " duration dont have 2 parts");
            }
            result.put(name, duration);
        }
        return result;      
	}
	
	public String getNextPage() {
	    Elements els = document.select(".b-browse-pages a");
	    for (Element el : els) {
	    	if (el.attr("title").equals("Next Page")) {
	    		return el.attr("abs:href");
	    	}
	    }
	    return null;
	}
}
