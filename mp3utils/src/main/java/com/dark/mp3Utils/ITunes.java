package com.dark.mp3Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;

public class ITunes implements Runnable {
    static Logger logger = Logger.getLogger(ITunes.class);
    
    
    private static ConcurrentLinkedQueue<String> queue = new ConcurrentLinkedQueue<String>();
    private static List<String> used = new ArrayList<String>();
    private static AtomicInteger count =  new AtomicInteger();

    private SimpleHttpClient client = new SimpleHttpClient();
    private Callback cb;
    private final static int limitUrls = 10000;    
    
    
    public interface Callback {
        public void result(String author, String title);
    }
    
    public ITunes(Callback cb) {
        this.cb = cb;
    }

    
    public void run() {
        addUrlToQueue("https://itunes.apple.com/us/genre/music/id34");
        
        while (true) {
            String url = queue.poll();
            if (url == null && used.size() >= limitUrls) {
                break;
            } 
            if (url != null) {
                try {
                    RequestResponce response = client.doGetRequest(url);
                    String html = SimpleHttpClient.httpResponseToString(response);
                    Document doc = Jsoup.parse(html, url); 
                    addUrlsToQueue(getAllUrls(doc));
                    if (url.contains("/artist/")) {
                        String artist = doc.select("h1").text();
                        for (Element el : doc.select(".tracklist-content-box td.name")) {
                            cb.result(artist, el.text());
                        }
                    }
                } catch (ClientProtocolException e) {
                    logger.info(e, e);
                } catch (IOException e) {
                    logger.info(e, e);
                }
            } else {
                try {Thread.sleep(1000);} catch (InterruptedException e) {logger.error(e, e);}
            }
        }

    }
    
    private List<String> getAllUrls(Document doc) {
        List<String> urls = new ArrayList<String>();
        Elements links = doc.select("a");
        for (Element link : links) {
            String url = link.attr("abs:href");
            if (url.toLowerCase().startsWith("https://itunes.apple.com/")) {
                urls.add(url);
            }
        }
        return urls;
    }

    
    private void addUrlsToQueue(List<String> urls) {
        for (String url : urls) {
            addUrlToQueue(url);
        }
    }
    
    private void addUrlToQueue(String url) {
        synchronized (queue) {
            if (!used.contains(url) && used.size() < limitUrls) {
                used.add(url);
                queue.add(url);
            }
        }
    }
    
}
