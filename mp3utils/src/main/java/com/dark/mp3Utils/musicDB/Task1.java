package com.dark.mp3Utils.musicDB;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.dark.http.SimpleHttpClient;
import com.dark.mp3.model.MusicAlbumService;
import com.dark.mp3.model.MusicAuthor;
import com.dark.mp3.model.MusicAuthorService;
import com.dark.mp3.model.MusicSongService;
import com.dark.mp3Utils.Mp3va;

public class Task1 implements Runnable {
    private static Logger logger = Logger.getLogger(Task1.class);
    
    private ThreadPoolExecutor threadPoolExecutor;
    private String url;
    private MusicAuthorService musicAuthorService;
    private MusicAlbumService musicAlbumService;
    private MusicSongService musicSongService;
    

    public Task1(ThreadPoolExecutor threadPoolExecutor, String url, MusicAuthorService musicAuthorService, MusicAlbumService musicAlbumService,
            MusicSongService musicSongService) {
        super();
        this.threadPoolExecutor = threadPoolExecutor;
        this.url = url;
        this.musicAuthorService = musicAuthorService;
        this.musicAlbumService = musicAlbumService;
        this.musicSongService = musicSongService;
    }


    public void run() {
        SimpleHttpClient httpClient = new SimpleHttpClient();
        try {
        	try {Thread.sleep(2000);} catch (InterruptedException e) {}
            String html = SimpleHttpClient.httpResponseToString(httpClient.doGetRequest(url));
            Mp3va mp3va = new Mp3va(html, url);
            Map<String, String> authorsMap = mp3va.getAuthors();
            for (String name : authorsMap.keySet()) {
                String url = authorsMap.get(name);
                MusicAuthor musicAuthor = musicAuthorService.getOrCreate(name);
                threadPoolExecutor.submit(new Task2(threadPoolExecutor, url, musicAuthor, musicAlbumService, musicSongService));
            }
            String nextPage = mp3va.getNextPage();
            if (nextPage != null) {
            	threadPoolExecutor.submit(new Task1(threadPoolExecutor, nextPage, musicAuthorService, musicAlbumService, musicSongService));
            }
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }
    }

}
