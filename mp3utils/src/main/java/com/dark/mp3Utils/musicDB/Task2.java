package com.dark.mp3Utils.musicDB;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.dark.http.SimpleHttpClient;
import com.dark.mp3.model.MusicAlbum;
import com.dark.mp3.model.MusicAlbumService;
import com.dark.mp3.model.MusicAuthor;
import com.dark.mp3.model.MusicSongService;
import com.dark.mp3Utils.Mp3va;

public class Task2 implements Runnable {
    private static Logger logger = Logger.getLogger(Task2.class);
    
    private ThreadPoolExecutor threadPoolExecutor;
    private String url;
    private MusicAuthor musicAuthor;
    private MusicAlbumService musicAlbumService;
    private MusicSongService musicSongService;
    

    public Task2(ThreadPoolExecutor threadPoolExecutor, String url, MusicAuthor musicAuthor, MusicAlbumService musicAlbumService,
            MusicSongService musicSongService) {
        super();
        this.threadPoolExecutor = threadPoolExecutor;
        this.url = url;
        this.musicAuthor = musicAuthor;
        this.musicAlbumService = musicAlbumService;
        this.musicSongService = musicSongService;
    }


    public void run() {
        SimpleHttpClient httpClient = new SimpleHttpClient();
        try {
            try {Thread.sleep(2000);} catch (InterruptedException e) {}
            String html = SimpleHttpClient.httpResponseToString(httpClient.doGetRequest(url));
            Mp3va mp3va = new Mp3va(html, url);
            Map<String, String> albumsMap = mp3va.getAlbums();
            for (String name : albumsMap.keySet()) {
                String url = albumsMap.get(name);
                MusicAlbum musicAlbum = musicAlbumService.getOrCreate(name, musicAuthor.getId());
                threadPoolExecutor.submit(new Task3(threadPoolExecutor, url, musicAuthor, musicAlbum, musicSongService));
            }
            String nextPage = mp3va.getNextPage();
            if (nextPage != null) {
            	threadPoolExecutor.submit(new Task2(threadPoolExecutor, nextPage, musicAuthor, musicAlbumService, musicSongService));
            }
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        }

    }

}
