package com.dark.mp3Utils.musicDB;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ThreadPoolExecutor;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.dark.http.SimpleHttpClient;
import com.dark.mp3.model.MusicAlbum;
import com.dark.mp3.model.MusicAuthor;
import com.dark.mp3.model.MusicSongService;
import com.dark.mp3Utils.Mp3va;

public class Task3 implements Runnable {
    private static Logger logger = Logger.getLogger(Task3.class);
    
    private ThreadPoolExecutor threadPoolExecutor;
    private String url;
    private MusicAuthor musicAuthor;
    private MusicAlbum musicAlbum;
    private MusicSongService musicSongService;
    

    public Task3(ThreadPoolExecutor threadPoolExecutor, String url, MusicAuthor musicAuthor, MusicAlbum musicAlbum,
            MusicSongService musicSongService) {
        super();
        this.threadPoolExecutor = threadPoolExecutor;
        this.url = url;
        this.musicAuthor = musicAuthor;
        this.musicAlbum = musicAlbum;
        this.musicSongService = musicSongService;
    }


    public void run() {
        SimpleHttpClient httpClient = new SimpleHttpClient();
        try {
        	try {Thread.sleep(2000);} catch (InterruptedException e) {}
            String html = SimpleHttpClient.httpResponseToString(httpClient.doGetRequest(url));
            Mp3va mp3va = new Mp3va(html, url);
            Map<String, Long> songsMap = mp3va.getSongs();
            for (String name : songsMap.keySet()) {
                Long duration = songsMap.get(name);
                musicSongService.getOrCreate(name, musicAlbum.getId(), duration);
            }
        } catch (ClientProtocolException e) {
            logger.warn(e, e);
        } catch (IOException e) {
            logger.warn(e, e);
        } catch (Throwable e) {
            logger.error(e, e);
        }

    }

}
