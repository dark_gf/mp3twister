package com.dark.mp3Utils;

import java.sql.SQLException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.dark.mp3.model.MusicAlbum;
import com.dark.mp3.model.MusicAlbumService;
import com.dark.mp3.model.MusicAuthor;
import com.dark.mp3.model.MusicAuthorService;
import com.dark.mp3.model.MusicSong;
import com.dark.mp3.model.MusicSongService;
import com.dark.mp3Utils.musicDB.Task1;
import com.dark.site.Config;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;


public class MusicDBMain {

	/**
	 * @param args
	 * @throws SQLException 
	 */
	public static void main(String[] args) throws SQLException {
		Config config = new Config();
		Dao<MusicAuthor, Long> daoAuthor = DaoManager.createDao(config.getJdbcPooledConnectionSource(), MusicAuthor.class);
		Dao<MusicAlbum, Long> daoAlbum = DaoManager.createDao(config.getJdbcPooledConnectionSource(), MusicAlbum.class);
		Dao<MusicSong, Long> daoSong = DaoManager.createDao(config.getJdbcPooledConnectionSource(), MusicSong.class);
		MusicAuthorService musicAuthorService = new MusicAuthorService(daoAuthor);
		MusicAlbumService musicAlbumService = new MusicAlbumService(daoAlbum);
		MusicSongService musicSongService = new MusicSongService(daoSong);
		
		String entryUrl = "http://www.mp3va.com/browse";
		
		BlockingQueue<Runnable> queue = new LinkedBlockingDeque<Runnable>();
		ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(1, 1, 0, TimeUnit.SECONDS, queue);
		threadPoolExecutor.submit(new Task1(threadPoolExecutor, entryUrl, musicAuthorService, musicAlbumService, musicSongService));
	}

}
