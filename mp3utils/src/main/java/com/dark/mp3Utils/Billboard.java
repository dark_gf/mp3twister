package com.dark.mp3Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;

public class Billboard {
    static Logger logger = Logger.getLogger(Billboard.class);
    
    private SimpleHttpClient client = new SimpleHttpClient();
    LinkedList<String> queue = new LinkedList<String>();
    List<String> used = new ArrayList<String>();
    public interface Callback {
        public void result(String author, String title);
    }
    
    public void getSongs(Callback callback) {
        addUrlToQueue("http://www.billboard.com/charts/hot-100");
        
        while (queue.size() > 0) {
            String url = queue.pollFirst();
            
            try {
                RequestResponce response = client.doGetRequest(url);
                String html = SimpleHttpClient.httpResponseToString(response);
                Document doc = Jsoup.parse(html, url); 
                
                //search for songs
                Elements divs = doc.select(".chart_listing article");
                for (Element div : divs) {
                    Element songEl = div.select("h1").first();
                    Element authorEl = div.select(".chart_info a").first();
                    if (songEl != null && authorEl != null) {
                        callback.result(authorEl.text(), songEl.text());
                    }
                }
                
                //search for paginator
                Elements pageEls = doc.select(".pager-item > a");
                for (Element pageEl : pageEls) {
                    if (!pageEl.text().contains("chart")) {
                        addUrlToQueue(pageEl.attr("abs:href"));
                    }
                }

                //search for other charts
                Element nextChartLink = doc.select(".next-chart > a").first();
                if (nextChartLink != null) {
                    addUrlToQueue(nextChartLink.attr("abs:href"));
                }
                
                //previous week chart
                Element prevChartLink = doc.select(".header_meta > .prev > a").first();
                if (prevChartLink != null) {
                    addUrlToQueue(prevChartLink.attr("abs:href"));
                }
                
            } catch (ClientProtocolException e) {
                logger.warn(e, e);
            } catch (IOException e) {
                logger.warn(e, e);
            }
        }
        
    }
    
    private void addUrlToQueue(String url) {
        if (!used.contains(url) && used.size() < 2000) {
            used.add(url);
            queue.add(url);
        }
    }

}
