package com.dark.http;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.config.RequestConfig.Builder;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.log4j.Logger;

public class SimpleHttpClient {

	private CloseableHttpClient httpClient;

	private Map<String, String> defaultHeaders = new HashMap<String, String>();

	static Logger logger = Logger.getLogger(SimpleHttpClient.class);

	private static volatile RequestMonitor monitorInstance;

	private static Boolean monitorLock = true;

	private static long readLimit = 5000000;

	private class RequestMonitor implements Runnable {

		private final Logger logger = Logger.getLogger(RequestMonitor.class);

		// request -> started timestamp
		private Hashtable<HttpUriRequest, Long> requests = new Hashtable<HttpUriRequest, Long>();

		private Long timeout = 30000l;

		private boolean run = true;

		public void run() {
			try {
				while (run) {
					Hashtable<HttpUriRequest, Long> requestsCopy = (Hashtable<HttpUriRequest, Long>) requests.clone();
					for (HttpUriRequest request : requestsCopy.keySet()) {
						Long timestamp = requestsCopy.get(request);
						if (request.isAborted()) {
							requests.remove(request);
						} else {
							if (System.currentTimeMillis() - timestamp > timeout) {
								request.abort();
								logger.debug(request.toString() + " is aborted");
							}
						}
					}

					// Iterator<Entry<HttpUriRequest, Long>> iter = requests.entrySet().iterator();
					// while (iter.hasNext()) {
					// Entry<HttpUriRequest, Long> entry = iter.next();
					// HttpUriRequest request = entry.getKey();
					// Long timestamp = entry.getValue();
					// if (request.isAborted()) {
					// iter.remove();
					// } else {
					// if (System.currentTimeMillis() - timestamp > timeout) {
					// request.abort();
					// logger.debug(request.toString() + " is aborted");
					// }
					// }
					// }

					try {
						Thread.sleep(2000);
						logger.debug("requests count in monitor " + requests.size());
					} catch (InterruptedException e) {
						logger.error(e, e);
					}
				}
			} catch (Throwable e) {
				logger.error(e, e);
			}
			logger.info("Request monitor shuting down");
		}

		public void addRequest(HttpUriRequest request) {
			requests.put(request, System.currentTimeMillis());
		}

		public void shutdown() {
			run = false;
		}

	}

	public class RequestResponce {

		private HttpUriRequest httpUriRequest;

		private CloseableHttpResponse closeableHttpResponse;

		public RequestResponce(HttpUriRequest httpUriRequest, CloseableHttpResponse closeableHttpResponse) {
			super();
			this.httpUriRequest = httpUriRequest;
			this.closeableHttpResponse = closeableHttpResponse;
		}

		public HttpUriRequest getRequest() {
			return httpUriRequest;
		}

		public CloseableHttpResponse getResponse() {
			return closeableHttpResponse;
		}
	}

	public SimpleHttpClient() {
		BasicHttpClientConnectionManager mgr = new BasicHttpClientConnectionManager();
		httpClient = HttpClients.custom().disableCookieManagement().setConnectionManager(mgr).build();
		defaultHeaders.put("User-Agent", "Mozilla/2.0 Some browser");
		defaultHeaders.put("Accept-Encoding", "gzip,deflate");
		defaultHeaders.put("Accept-Language", "en-US,en;q=0.8");
	}

	public RequestResponce doGetRequest(String url) throws ClientProtocolException, IOException {
		logger.debug("GET: " + url);
		RequestConfig config = createDefaultBuilder().build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(config);
		applyDefaultHeaders(httpGet);
		addToMonitor(httpGet);
		return new RequestResponce(httpGet, httpClient.execute(httpGet));
	}

	public RequestResponce doGetRequest(String url, Integer timeout) throws ClientProtocolException, IOException {
		logger.debug("GET: " + url);
		RequestConfig config = createDefaultBuilder().setConnectTimeout(timeout).setSocketTimeout(timeout).build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(config);
		applyDefaultHeaders(httpGet);
		addToMonitor(httpGet);
		return new RequestResponce(httpGet, httpClient.execute(httpGet));
	}

	public RequestResponce doGetRequest(String url, Integer timeout, HttpHost proxy) throws ClientProtocolException, IOException {
		logger.debug("GET with proxy: " + url);
		RequestConfig config = createDefaultBuilder().setConnectTimeout(timeout).setSocketTimeout(timeout).setProxy(proxy).build();
		HttpGet httpGet = new HttpGet(url);
		httpGet.setConfig(config);
		applyDefaultHeaders(httpGet);
		addToMonitor(httpGet);
		return new RequestResponce(httpGet, httpClient.execute(httpGet));
	}
	
	public RequestResponce doPostRequest(String url, List<NameValuePair> nvps, Integer timeout) throws ClientProtocolException, IOException {
		logger.debug("GET with proxy: " + url);
		RequestConfig config = createDefaultBuilder().setConnectTimeout(timeout).setSocketTimeout(timeout).build();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setConfig(config);
        
        httpPost.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
        
		applyDefaultHeaders(httpPost);
		addToMonitor(httpPost);
		return new RequestResponce(httpPost, httpClient.execute(httpPost));
	}

	/**
	 * Simple method to convert CloseableHttpResponse to String, please note - connection closed and resources realized for that HttpResponse even if exception thrown
	 * 
	 * @param response
	 *        - CloseableHttpResponse that needed to be converted to String
	 * @return String - body of HttpResponse
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	public static String httpResponseToString(RequestResponce response) throws IllegalStateException, IOException {
		String result = null;
		InputStream inputStream = null;
		try {
			inputStream = response.getResponse().getEntity().getContent();
			BufferedInputStream buffInputStream = new BufferedInputStream(inputStream);
			StringBuilder builder = new StringBuilder();
			byte[] buffer = new byte[1024];
			for (;;) {
				int rsz = buffInputStream.read(buffer);
				if (rsz < 0) break;
				if (builder.length() + rsz > readLimit) {
					buffInputStream.close();
					throw new IOException("Read limit " + readLimit + " reached");
				}
				builder.append(new String(buffer));
			}
			result = builder.toString().trim();
		} finally {
			response.getRequest().abort();
			IOUtils.closeQuietly(inputStream);
			response.getResponse().close();
		}
		return result;
	}

	private void applyDefaultHeaders(HttpUriRequest request) {
		for (String key : defaultHeaders.keySet()) {
			request.setHeader(key, defaultHeaders.get(key));
		}
	}

	private Builder createDefaultBuilder() {
		return RequestConfig.custom().setConnectTimeout(3000).setSocketTimeout(20000);
	}

	private void addToMonitor(HttpUriRequest request) {
		if (monitorInstance == null) {
			synchronized (monitorLock) {
				if (monitorInstance == null) {
					monitorInstance = new RequestMonitor();
					(new Thread(monitorInstance)).start();
				}
			}
		}
		monitorInstance.addRequest(request);
	}

	public static void shutdownMonitor() {
		if (monitorInstance != null) {
			synchronized (monitorLock) {
				if (monitorInstance == null) {
					monitorInstance.shutdown();
				}
			}
		}
	}
}
