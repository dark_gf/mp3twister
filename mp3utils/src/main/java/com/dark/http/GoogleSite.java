package com.dark.http;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.dark.site.Google;
import com.dark.site.Utils;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.Response;

public class GoogleSite {
    private List<String> links = new ArrayList<String>();
    private int offset = 0;
    private GoogleSiteCompletionHandler handler;
    private String query;
    
	public GoogleSite(AsyncHttpClient httpClient, String query, GoogleSiteCompletionHandler responseHandler) {
		int ttl = 20;
		handler = responseHandler;
		this.query = query;
		String url = generateUrl(query, offset);
		Request request = httpClient.prepareGet(url).build();
		sendRequest(httpClient, request, ttl);
	}
	
	protected void sendRequest(final AsyncHttpClient httpClient, final Request request, final int ttl) {
        if (ttl <= 0) {
            handler.complete(links);
            return;
        }
	    try {
	        System.out.println("Executing: " + request.getUrl());
			httpClient.executeRequest(request, new AsyncCompletionHandler<Response>() {
				@Override
				public Response onCompleted(Response response) throws Exception {
					//saveToFile(Long.toString(System.currentTimeMillis()) + ".txt", response.getResponseBody());
				    Google google = new Google(response.getResponseBody());
					
					if (google.getResult() == google.RESULT_OK) {
					    links.addAll(google.getLinks());
					    if (google.hasNextPage()) {
					        offset += 100;
					        Request requestNextPage = httpClient.prepareGet(generateUrl(query, offset)).build();
					        sendRequest(httpClient, requestNextPage, ttl);
					    } else {
					        handler.complete(links);
					    }
					} else {
				        sendRequest(httpClient, request, ttl - 1);
					}
					
					return response;
				}
				
			    @Override
			    public void onThrowable(Throwable t) {
			        t.printStackTrace();
		        	sendRequest(httpClient, request, ttl - 1);
			    }
			});
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected String generateUrl(String query, int offset) {
	    return "http://www.google.com/search?q=" + Utils.encode(query) + "&num=100&hl=en&prmd=imvnsu&start=" + Integer.toString(offset) + "&sa=N";
	}
	
	protected void saveToFile(String filename, String data) {
	    try {        
    	    File file = new File(filename);
            
            if (!file.exists()) {
                file.createNewFile();
            }
    
            FileWriter fw = new FileWriter(file.getAbsoluteFile());
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
	}

}
