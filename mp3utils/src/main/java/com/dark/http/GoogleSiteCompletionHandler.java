package com.dark.http;

import java.util.List;

public interface GoogleSiteCompletionHandler {
	public void complete(List<String> links);
}
