package com.dark.mp3.model;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.Timestamp;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;

import org.apache.log4j.Logger;

import com.dark.site.Utils;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "mp3")
public class Mp3 implements Serializable {
    private static Logger logger = Logger.getLogger(Mp3.class);
    
    @DatabaseField(canBeNull = false, dataType = DataType.LONG_STRING)
    private String url;
    
    @DatabaseField(canBeNull = true, dataType = DataType.LONG_STRING)
    private String author;
    
    @DatabaseField(canBeNull = true, dataType = DataType.LONG_STRING)
    private String album;

    @DatabaseField(canBeNull = true, dataType = DataType.LONG_STRING)
    private String title;
    
    @DatabaseField(canBeNull = true)
    private Integer bitrate;

    @DatabaseField(canBeNull = true)
    private Long size;
    
    @DatabaseField(canBeNull = true)
    private Integer freq;
    
    @DatabaseField(canBeNull = true)
    private Long duration;

    @DatabaseField(canBeNull = false, columnName = "date_created")
    private Timestamp dateCreated;
    
    @DatabaseField(canBeNull = false, columnName = "date_updated")
    private Timestamp dateUpdated;
    
    @DatabaseField(canBeNull = false)
    private Integer status;
    
    @DatabaseField(canBeNull = false, columnName = "source_url_hash")
    private String sourceUrlHash;

    @DatabaseField(canBeNull = false, id = true)
    private String hash;
    
    
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        setHash(Utils.getMd5(url));
    }
    
    public String getPath() {
        int pos = url.lastIndexOf("/");
        if (pos > 0) {
            return url.substring(0, pos + 1);
        }
        return url;
    }
    
    public String getShortPath() {
        String path = getPath();
        if (path.length() < 9) return path;
        if (path.substring(0, 7).toLowerCase().equals("http://")) {
            path = path.substring(7);
        }
        if (path.substring(0, 8).toLowerCase().equals("https://")) {
            path = path.substring(8);
        }
        int posFirst = path.indexOf("/");
        int posLast = path.lastIndexOf("/");
        if (posFirst != posLast) {
            path = path.substring(0, posFirst + 1) + "..." + path.substring(posLast);
        }
        
        return path;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getBitrate() {
        return bitrate;
    }

    public void setBitrate(Integer bitrate) {
        this.bitrate = bitrate;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public Integer getFreq() {
        return freq;
    }

    public void setFreq(Integer freq) {
        this.freq = freq;
    }

    public Long getDuration() {
        return duration;
    }
    
    public Long getPossibleDuration() {
        if (duration == null) {
            if (getBitrate() != null && getBitrate() != 0 && getSize() != null) {
                return getSize() / (getBitrate()/8);
            } else {
                return 0l;
            }
        } else {
            return duration;
        }
    }
    
    public String getFormattedDuration() {
        Long seconds = getPossibleDuration();
        return String.format("%d:%02d", seconds / 60, seconds % 60);
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public Timestamp getDateCreated() {
        return dateCreated;
    }

    public void setDateCreated(Timestamp dateCreated) {
        this.dateCreated = dateCreated;
    }

    public Timestamp getDateUpdated() {
        return dateUpdated;
    }

    public void setDateUpdated(Timestamp dateUpdated) {
        this.dateUpdated = dateUpdated;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getSourceUrlHash() {
        return sourceUrlHash;
    }

    public void setSourceUrlHash(String sourceUrlHash) {
        this.sourceUrlHash = sourceUrlHash;
    }
    
    private void setHash(String hash) {
        this.hash = hash;
    }
    
    public String getHash() {
    	return this.hash;
    }
    
    public String getName() {
        String result;
        int index = this.url.lastIndexOf("/");
        if (index >= 0) {
            result = this.url.substring(index + 1);
        } else {
            result = this.url;
        }
        try {
            result = URLDecoder.decode(result, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            logger.error(e, e);
            result = "";
        }
        return result;
    }

    public static Mp3 parse(AudioFileFormat audioFileFormat, Long size) {
//      mp3.crc : false
//      mp3.id3tag.v2.version : 3
//      mp3.copyright : false
//      mp3.padding : false
//      album : Revelations of a Wife
//      mp3.id3tag.length : 0
//      mp3.channels : 1
//      mp3.version.mpeg : 2
//      mp3.framerate.fps : 38.28125
//      mp3.framesize.bytes : 204
//      author : Adele Garrison
//      title : 19 - Lillian Underwood\'s Story
//      mp3.version.layer : 3
//      mp3.frequency.hz : 22050
//      mp3.header.pos : 260
//      mp3.bitrate.nominal.bps : 64000
//      mp3.vbr.scale : 0
//      mp3.version.encoding : MPEG2L3
//      mp3.mode : 3
//      mp3.vbr : false
//      mp3.id3tag.v2 : java.io.ByteArrayInputStream@5e0eb724
//      comment : http://www.archive.org/details/revelations_wife_mr_0906_librivox
//      mp3.original : true
        
        
        Mp3 mp3 = new Mp3();
        Map properties = audioFileFormat.properties();
        mp3.setAuthor((String) properties.get("author"));
        mp3.setAlbum((String) properties.get("album"));
        mp3.setTitle((String) properties.get("title"));
        mp3.setBitrate((Integer) properties.get("mp3.bitrate.nominal.bps"));
        mp3.setFreq((Integer) properties.get("mp3.frequency.hz"));
        mp3.setDuration((Long) properties.get("duration"));
        mp3.setSize(size);
        Timestamp now = new Timestamp(System.currentTimeMillis());
        mp3.setDateCreated(now);
        mp3.setDateUpdated(now);
        mp3.setStatus(1);
        
        if (mp3.getAlbum() == null && mp3.getAuthor() == null && mp3.getTitle() == null) {
            mp3 = null;
        } 
        return mp3;
    }
    
    
}
