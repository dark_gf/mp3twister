package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;

public class UrlService extends AbstractService {
    static Logger logger = Logger.getLogger(UrlService.class);
    
    Dao<Url, String> dao;
    
    public UrlService(Dao<Url, String> dao) {
        this.dao = dao;
    }
    
    public void insertBatch(final List<Url> list) throws Exception {
    	dao.callBatchTasks(new Callable<Void>() {
    	    public Void call() throws Exception {
    	        for (Url url : list) {
    	        	dao.createOrUpdate(url);
    	        }
				return null;
    	    }
    	});
    }
    
}
