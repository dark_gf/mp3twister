package com.dark.mp3.model;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.SelectArg;

public class MusicAlbumService {
    static Logger logger = Logger.getLogger(MusicAlbumService.class);

    Dao<MusicAlbum, Long> dao;

    public MusicAlbumService(Dao<MusicAlbum, Long> dao) {
        this.dao = dao;
    }
    
    public MusicAlbum getOrCreate(String name, Long authorId) {
        MusicAlbum result = null;
        try {
            SelectArg nameArg = new SelectArg(name);
            result = dao.queryBuilder().where().eq("name", nameArg).and().eq("author_id", authorId).queryForFirst();
            if (result == null) {
                result = new MusicAlbum();
                result.setAuthorId(authorId);
                result.setName(name);
                dao.createIfNotExists(result);
            }
        } catch (SQLException e) {
            logger.error(e, e);
        } catch (Throwable e) {
            logger.error(e, e);
        }
        
        return result;
    }
    
}
