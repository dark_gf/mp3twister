package com.dark.mp3.model;

import java.sql.Timestamp;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "mp3_check")
public class Mp3Check {
    @DatabaseField(generatedId = false)
    private Long id;
    
    @DatabaseField(canBeNull = false)
    private Timestamp date;

    @DatabaseField(canBeNull = false)
    private Integer status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    
    
    
}
