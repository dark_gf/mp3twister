package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;

public class Mp3Service extends AbstractService {
    static Logger logger = Logger.getLogger(Mp3Service.class);
    
    Dao<Mp3, String> dao;
    
    public Mp3Service(Dao<Mp3, String> dao) {
        this.dao = dao;
    }
    
    public List<Mp3> getFromDateUpdated(Timestamp fromTimestamp, long limit) {
        try {
            return dao.queryBuilder().limit(limit).orderBy("date_updated", true).where().gt("date_updated", fromTimestamp).query();
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }
    
    public Mp3 getByUrl(String url) {
        Mp3 result = null;
        try {
            result = dao.queryBuilder().where().eq("hash", Utils.getMd5(url)).queryForFirst();
        } catch (SQLException e) {
            logger.error(e);
        }
        return result;
    }
    
    public List<Mp3> getByIds(List<Long> ids) {
        List<Mp3> result = new ArrayList<Mp3>();
        if (ids.size() == 0) {
        	return result;
        }
        try {
            result = dao.queryBuilder().orderByRaw("FIELD(id, " + StringUtils.join(ids, ", ") +")").where().in("id", ids).query();
        } catch (SQLException e) {
            logger.error(e, e);
        }
        return result;
    }
    
    public Mp3 getById(Long id) {
        try {
            return dao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            logger.error(e, e);
        }
        return null;
    }
    
    public void update(Mp3 mp3) {
        mp3.setDateUpdated(new Timestamp(System.currentTimeMillis()));
        try {
            dao.update(mp3);
        } catch (SQLException e) {
            logger.error(e, e);
        }
    }

	public void insertBatch(final ArrayList<Mp3> list) throws Exception {
    	dao.callBatchTasks(new Callable<Void>() {
    	    public Void call() throws Exception {
    	        for (Mp3 mp3 : list) {
    	        	dao.createOrUpdate(mp3);
    	        }
				return null;
    	    }
    	});
	}
}
