package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.LockSupport;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;

public class Mp3CheckService {
    private static Logger logger = Logger.getLogger(Mp3CheckService.class);
    private static ConcurrentHashMap<String, Long> locks = new ConcurrentHashMap <String, Long>();
    
    Dao<Mp3Check, Long> dao;
    
    public Mp3CheckService(Dao<Mp3Check, Long> dao) {
        this.dao = dao;
    }
    
    public Mp3Check inserNewCheck(Long id, int status) {
        Mp3Check mp3FileCheck = new Mp3Check();
        
        mp3FileCheck.setId(id);
        mp3FileCheck.setDate(new Timestamp(System.currentTimeMillis()));
        mp3FileCheck.setStatus(status);
        
        try {
            dao.create(mp3FileCheck);
        } catch (SQLException e) {
            logger.error(e, e);
        }
        
        return mp3FileCheck;
    }
    
    public Mp3Check getLastCheck(Long id) {
        try {
            return dao.queryBuilder().limit(1l).orderBy("date", false).where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }
    
    public List<Mp3Check> getChecks(Long id) {
        try {
            return dao.queryBuilder().orderBy("date", false).where().eq("id", id).query();
        } catch (SQLException e) {
            logger.error(e);
        }
        return null;
    }
    
    
    public static boolean getLock(String lock, Long delta) {
        boolean result = true;
        if (locks.containsKey(lock)) {
            synchronized (locks) {
                Long time = locks.get(lock);
                if (System.currentTimeMillis() - time > delta) {
                    locks.put(lock, System.currentTimeMillis());
                } else {
                    result = false;
                }
            }
            synchronized (locks) {
                if (locks.size() > 1000) {
                    for (String l : locks.keySet()) {
                        if (System.currentTimeMillis() - locks.get(l) > 60000) {
                            locks.remove(l);
                        }
                    }
                }
            }
        } else {
            locks.put(lock, System.currentTimeMillis());
        }
        return result;
    }
}
