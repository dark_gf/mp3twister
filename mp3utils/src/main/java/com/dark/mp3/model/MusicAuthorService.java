package com.dark.mp3.model;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.SelectArg;


public class MusicAuthorService extends AbstractService {
		private static Logger logger = Logger.getLogger(MusicAuthorService.class);

		Dao<MusicAuthor, Long> dao;

		public MusicAuthorService(Dao<MusicAuthor, Long> dao) {
			this.dao = dao;
		}
		
		public MusicAuthor getOrCreate(String name) {
		    MusicAuthor result = null;
            try {
                SelectArg nameArg = new SelectArg(name);
                result = dao.queryBuilder().where().eq("name", nameArg).queryForFirst();
                if (result == null) {
                    result = new MusicAuthor();
                    result.setName(name);
                    dao.createIfNotExists(result);
                }
            } catch (SQLException e) {
                logger.error(e, e);
            }
		    
		    return result;
		}
}
