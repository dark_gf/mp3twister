package com.dark.mp3.model;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.stmt.PreparedQuery;
import com.j256.ormlite.stmt.QueryBuilder;

public class Url implements Serializable {
    @DatabaseField(canBeNull = false, id = true)
    private String md5;
    
    @DatabaseField(canBeNull = false, dataType = DataType.LONG_STRING)
    private String url;
    
    @DatabaseField(canBeNull = false, index = true)
    private Long timestamp;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
        this.md5 = Utils.getMd5(url);
        if (timestamp == null) updateTimestamp();
    }

    public String getMd5() {
        return md5;
    }

    public Long getTimestamp() {
        return timestamp;
    }
    
    public void updateTimestamp() {
        this.timestamp = System.currentTimeMillis();
    }
    
    public static Url get(String url, Dao<Url, String> dao) {
        Url urlObj = null;
        try {
            urlObj = dao.queryForId(Utils.getMd5(url));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return urlObj;
    }
    
}
