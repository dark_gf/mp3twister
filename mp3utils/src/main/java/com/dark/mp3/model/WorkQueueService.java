package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;

public class WorkQueueService extends AbstractService {
    private static Logger logger = Logger.getLogger(WorkQueueService.class);
    
    Dao<WorkQueue, Long> dao;
    
    public WorkQueueService(Dao<WorkQueue, Long> dao) {
        this.dao = dao;
    }
    
    public WorkQueue createNewWork(String comment) {
		WorkQueue workQueue = new WorkQueue();
		workQueue.setTime(new Timestamp(System.currentTimeMillis()));
		workQueue.setStatus(WorkQueue.STATUS_IN_WORK);
		workQueue.setComment(comment);
		try {
			dao.create(workQueue);
		} catch (SQLException e) {
			logger.error(e, e);
		}
		return workQueue;
    }
    
    public WorkQueue findById(Integer id) {
        try {
            return dao.queryBuilder().where().eq("id", id).queryForFirst();
        } catch (SQLException e) {
            logger.error(e, e);
        }
        return null;
    }
    
    /**
     * Search for unfinished work, unfinished work is work with status STATUS_IN_WORK
     * @return WorkQueue
     */
    public WorkQueue findStalledWork() {
        try {
            return dao.queryBuilder().where().eq("status", WorkQueue.STATUS_IN_WORK).and().lt("time", new Timestamp(System.currentTimeMillis() - 24 * 60 * 60 * 1000)).queryForFirst();
        } catch (SQLException e) {
            logger.error(e, e);
        }
        return null;
    }
    
    public void setDone(WorkQueue workQueue) {
    	workQueue.setStatus(WorkQueue.STATUS_DONE_OK);
    	update(workQueue);
    }
    
    public void addCommentAndUpdate(WorkQueue workQueue, String comment) {
    	String newComment = workQueue.getComment() + "\n" + comment;
    	workQueue.setComment(newComment);
    	update(workQueue);
    }
    
    private void update(WorkQueue workQueue) {
    	workQueue.setTime(new Timestamp(System.currentTimeMillis()));
    	try {
			dao.update(workQueue);
		} catch (SQLException e) {
			logger.error(e, e);
		}
    }
    
}
