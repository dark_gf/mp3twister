package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;


public class ConfigService extends AbstractService {
	private static final String FIELD_LAST_WORD_USED = "last_word_used";
    private static final String FIELD_LAST_DATE_INDEXER_USED = "last_date_indexer_used";
    
    Dao<Config, String> dao;
    
	static Logger logger = Logger.getLogger(ConfigService.class);

    public ConfigService(Dao<Config, String> dao) {
		this.dao = dao;
	}
	
    public void setLastWordUsed(Long value) {
        Config config = new Config(FIELD_LAST_WORD_USED);
        config.setValue(Long.toString(value));
        try {
            dao.createOrUpdate(config);
        } catch (SQLException e) {
        	logger.error(e);
        }
    }

    public Long getLastWordUsed() {
        Config config = null;
        try {
            config = dao.queryForId(FIELD_LAST_WORD_USED);
        } catch (SQLException e) {
            logger.error(e);
        }
        if (config == null) {
            config = new Config(FIELD_LAST_WORD_USED);
            config.setValue("0");
            try {
                dao.createOrUpdate(config);
            } catch (SQLException e) {
                logger.error(e);
            }
        }
        return Long.parseLong(config.getValue());
    }
    
    
    public Timestamp getLastDateIndexerUsed() {
        Config config = null;
        try {
            config = dao.queryForId(FIELD_LAST_DATE_INDEXER_USED);
        } catch (SQLException e) {
            logger.error(e);
        }
        if (config == null) {
            config = new Config(FIELD_LAST_DATE_INDEXER_USED);
            config.setValue("1");
            try {
                dao.createOrUpdate(config);
            } catch (SQLException e) {
                logger.error(e);
            }
        }        
        return new Timestamp(Long.valueOf(config.getValue()));
    }

    public void setLastDateIndexerUsed(Timestamp value) {
        Config config = new Config(FIELD_LAST_DATE_INDEXER_USED);
        config.setValue(String.valueOf(value.getTime()));
        try {
            dao.createOrUpdate(config);
        } catch (SQLException e) {
            logger.error(e);
        }
    }    
	
}
