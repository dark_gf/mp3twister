package com.dark.mp3.model;

import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.SelectArg;

public class MusicSongService extends AbstractService {
    static Logger logger = Logger.getLogger(MusicSongService.class);

    Dao<MusicSong, Long> dao;

    public MusicSongService(Dao<MusicSong, Long> dao) {
        this.dao = dao;
    }
    
    public MusicSong getOrCreate(String name, Long albumId, Long duration) {
        MusicSong result = null;
        try {
            SelectArg nameArg = new SelectArg(name);
            result = dao.queryBuilder().where().eq("name", nameArg).and().eq("album_id", albumId).queryForFirst();
            if (result == null) {
                result = new MusicSong();
                result.setAlbumId(albumId);
                result.setName(name);
                result.setDuration(duration);
                dao.createIfNotExists(result);
            }
        } catch (SQLException e) {
            logger.error(e, e);
        } catch (Throwable e) {
            logger.error(e, e);
        }
        
        return result;
    }    
}
