package com.dark.mp3.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;


@DatabaseTable(tableName = "music_author")
public class MusicAuthor {
	@DatabaseField(generatedId = true)
	private Long id;
	
	@DatabaseField(canBeNull = false)
	private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }
}
