package com.dark.mp3.model;

import com.j256.ormlite.field.DatabaseField;


public class Trend {
    @DatabaseField(canBeNull = false, id = true)
    private Integer id;
    
    @DatabaseField(canBeNull = false)
    private String word;
    
    @DatabaseField(canBeNull = false, index = true)
    private Integer weight;

	
	public String getWord() {
		return word;
	}

	
	public void setWord(String word) {
		this.word = word;
	}

	
	public Integer getWeight() {
		return weight;
	}

	
	public void setWeight(Integer weight) {
		this.weight = weight;
	}

	
	public Integer getId() {
		return id;
	}
    
    

}
