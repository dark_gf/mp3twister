package com.dark.mp3.model;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "words_eng")
public class Word {
	@DatabaseField(generatedId = true)
	private Long id;
	
	@DatabaseField(canBeNull = false)
	private String text;
	
	public Word() {
		
	}
	
	public String getText() {
		return text;
	}
	
	public Long getId() {
		return id;
	}
	
	public static synchronized List<Word> getWords(Dao<Word, Long> dao, long fromId, long limit) {
		try {
			return dao.queryBuilder().limit(limit).orderBy("id", true).where().gt("id", fromId).query();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return new ArrayList<Word>();
	}
	
}
