package com.dark.mp3.model;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "work_queue")
public class WorkQueue {
	public static final Integer STATUS_IN_WORK = 0;
	public static final Integer STATUS_DONE_OK = 1;
	public static final Integer STATUS_DONE_ERROR = 2;
	
	@DatabaseField(generatedId = true)
	private Long id;
	
	@DatabaseField(canBeNull = false)
	private Timestamp time;
	
	@DatabaseField(canBeNull = false)
	private Integer status;
	
	@DatabaseField(canBeNull = false)
	private String comment;
	
	public WorkQueue() {
		
	}

	public Timestamp getTime() {
		return time;
	}

	public void setTime(Timestamp time) {
		this.time = time;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getId() {
		return id;
	}

}
