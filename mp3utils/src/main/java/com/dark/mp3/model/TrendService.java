package com.dark.mp3.model;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.SelectArg;
import com.j256.ormlite.stmt.UpdateBuilder;

public class TrendService extends AbstractService {

	static Logger logger = Logger.getLogger(TrendService.class);

	Dao<Trend, Integer> dao;

	public TrendService(Dao<Trend, Integer> dao) {
		this.dao = dao;
	}

	public void insertTrend(String word) {
		try {
			SelectArg selectArg = new SelectArg();
			selectArg.setValue(word);
			if (dao.queryBuilder().where().eq("word", selectArg).queryForFirst() == null) {
				Trend trend = new Trend();
				trend.setWord(word);
				trend.setWeight(0);
				dao.create(trend);
			}
		} catch (SQLException e) {
			logger.error(e, e);
		}
	}

	public void shuffle() {
		UpdateBuilder<Trend, Integer> updateBuilder = dao.updateBuilder();
		try {
			updateBuilder.updateColumnExpression("weight", "FLOOR( 1 + RAND( ) *10000 )");
			logger.info(updateBuilder.toString());
			updateBuilder.update();
		} catch (SQLException e) {
			logger.error(e, e);
		}
	}

	public List<Trend> getTrends(int start, int limit) {
		try {
			return dao.queryBuilder().offset(Long.valueOf(start)).limit(Long.valueOf(limit)).orderBy("weight", true).query();
		} catch (SQLException e) {
			logger.error(e, e);
		}
		return null;
	}

	public List<Trend> getRandom(int limit) {
		try {
			//return dao.queryRaw("SELECT * FROM `trend` ORDER BY ").orderByRaw(" RANDOM()").limit(Long.valueOf(limit)).query();
			return dao.queryBuilder().orderByRaw("RAND()").limit(Long.valueOf(limit)).query();
		} catch (SQLException e) {
			logger.error(e, e);
		}
		return null;
	}

	public static Map<String, String> convertToUrlTrend(List<Trend> trends) {
		Map<String, String> map = new HashMap<String, String>();
		for (Trend trend : trends) {
			map.put("/" + Utils.encode(trend.getWord()) + ".html", trend.getWord());
		}
		return map;
	}

	
}
