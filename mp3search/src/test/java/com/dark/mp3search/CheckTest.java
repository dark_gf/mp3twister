package com.dark.mp3search;

import static org.junit.Assert.*;

import org.junit.Test;

import com.dark.mp3.model.Mp3;

public class CheckTest {

    @Test
    public void test() {
        String url = "http://www.bitlordsearch.com/bl/freemiumbl.php?name=Lana+Del+Rey+-+Summertime+Sadness+%28Cedric+Gervais+Remix%29.mp3&magnet=magnet%3A%3Fxt%3Durn%3Abtih%3Aaad109ba882c318634be07f91a3472a04c4754ea%26dn%3DLana%2BDel%2BRey%2B-%2BSummertime%2BSadness%2B%2528Cedric%2BGervais%2BRemix%2529.mp3";
        CheckMp3 checkMp3 = new CheckMp3(0l, url, "", null, null);
        Mp3 mp3 = checkMp3.parseMp3();
        assertNull(mp3);
    }

}
