package com.dark.mp3search;

import static org.junit.Assert.*;

import org.junit.Test;

import com.dark.mp3.model.Mp3;

public class Mp3Test {

    @Test
    public void testGetName() {
        Mp3 mp3File = new Mp3();
        mp3File.setUrl("http://somedomain/someurl/nameofsong%3A.mp3");
        assertEquals("nameofsong:.mp3", mp3File.getName());
    }

}
