package com.dark.mp3search;

import static org.junit.Assert.*;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.junit.Test;

import com.dark.mp3.model.ConfigService;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.dark.site.Config;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;

public class Mp3ServiceTest {

    @Test
    public void test() throws SQLException {
        Config config = new Config();
        Dao<Mp3, String> dao = DaoManager.createDao(config.getJdbcPooledConnectionSource(), Mp3.class);
        Mp3Service mp3FileService = new Mp3Service(dao);
        //TODO: need to test on empty db with fixtures
        //assertEquals(10, mp3FileService.getFromDateUpdated(new Timestamp(1), 10).size());
        
//        Long time = System.currentTimeMillis() / 1000;
//        Timestamp timestamp1 = new Timestamp(time);
//        Timestamp timestamp2 = new Timestamp(time + 1);
//        System.out.println(timestamp1);
//        System.out.println(timestamp2);
    }
    
    @Test
    public void testGetByIds() throws SQLException {
        Config config = new Config();
        Dao<Mp3, String> dao = DaoManager.createDao(config.getJdbcPooledConnectionSource(), Mp3.class);
        Mp3Service mp3FileService = new Mp3Service(dao);
        //TODO: need to test on empty db with fixtures
//        List<Long> ids = new ArrayList<Long>();
//        ids.add(1l);
//        ids.add(3l);
//        ids.add(2l);
//        List<Mp3> mp3Files = mp3FileService.getByIds(ids);
//        Iterator<Mp3> iterator = mp3Files.iterator();
//        assertEquals(new Long(1), iterator.next().getId());
//        assertEquals(new Long(3), iterator.next().getId());
//        assertEquals(new Long(2), iterator.next().getId());
    }

}
