package com.dark.mp3search;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.sql.SQLException;

import org.junit.Test;

import com.dark.mp3.model.Mp3CheckService;


public class Mp3CheckServiceTest {

    @Test
    public void test() throws SQLException, InterruptedException {
        assertTrue(Mp3CheckService.getLock("test", 1000l));
        assertFalse(Mp3CheckService.getLock("test", 1000l));
        Thread.sleep(1001);
        assertTrue(Mp3CheckService.getLock("test", 1000l));
        assertFalse(Mp3CheckService.getLock("test", 1000l));
    }
    
}
