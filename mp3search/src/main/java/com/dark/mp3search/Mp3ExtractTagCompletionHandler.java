package com.dark.mp3search;

import java.io.File;
import java.net.URI;
import java.util.List;

public interface Mp3ExtractTagCompletionHandler {
    public void complete(File file, URI uri);
}
