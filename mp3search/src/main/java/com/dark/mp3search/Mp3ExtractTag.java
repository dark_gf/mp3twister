package com.dark.mp3search;

import java.io.BufferedOutputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.HttpResponseBodyPart;
import com.ning.http.client.Request;
import com.ning.http.client.Response;

public class Mp3ExtractTag {
    private Mp3ExtractTagCompletionHandler handler;
    private File file;
    private BufferedOutputStream writer;
    public static AtomicInteger countActiveRequests = new AtomicInteger();
    
    public Mp3ExtractTag(AsyncHttpClient httpClient, String url, Mp3ExtractTagCompletionHandler responseHandler) {
        int ttl = 3;
        handler = responseHandler;
        Request request = httpClient.prepareGet(url).build();
        sendRequest(httpClient, request, ttl);
    }
    
    protected void sendRequest(final AsyncHttpClient httpClient, final Request request, final int ttl) {
        if (ttl <= 0) {
            closeWriter();
            removeFile();
            return;
        }
        openNewWriter();
        try {
            System.out.println("Downloading mp3: " + request.getUrl());
            countActiveRequests.addAndGet(1);
            httpClient.executeRequest(request, new AsyncCompletionHandler<Response>() {
                @Override
                public STATE onBodyPartReceived(HttpResponseBodyPart bodyPart) throws Exception {
                    write(bodyPart.getBodyPartBytes());
                    return STATE.CONTINUE;
                }
                
                @Override
                public Response onCompleted(Response response) throws Exception {
                    closeWriter();
                    if (file != null) {
                        handler.complete(file, response.getUri());
                    }
                    countActiveRequests.addAndGet(-1);
                    return response; 
                }
                
                @Override
                public void onThrowable(Throwable t) {
                    t.printStackTrace();
                    countActiveRequests.addAndGet(-1);
                    closeWriter();
                    removeFile();
                    sendRequest(httpClient, request, ttl - 1);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    protected void openNewWriter() {
        closeWriter();
        removeFile();
        
        file = new File("temp/" + Long.toString(System.currentTimeMillis()) + "-" + Long.toString(System.nanoTime()) + ".mp3");
        try {
            file.getParentFile().mkdirs();
            file.createNewFile();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            writer = new BufferedOutputStream(new FileOutputStream(file), 32768);
        } catch (IOException e) {
            System.err.println("Can't create writer!");
            e.printStackTrace();
        }
    }
    
    protected void closeWriter() {
        if (writer != null) {
            try {
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        writer = null;
    }
    
    protected void removeFile() {
        if (file != null) {
            file.delete();
        }
        file = null;        
    }
    
    protected void write(byte[] bs) {
        if (writer != null) {
            try {
                writer.write(bs);
            } catch (IOException e) {
                System.err.println("Can't write!");
                e.printStackTrace();
            }
        } else {
            System.err.println("Something went wrong: writer is null");
        }
    }
}



