package com.dark.mp3search;

import java.util.List;

public interface Mp3LinksExtractCompletionHandler {
	public void complete(List<String> links);
}
