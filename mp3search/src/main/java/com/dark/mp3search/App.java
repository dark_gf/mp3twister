package com.dark.mp3search;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.dark.mp3.model.Word;
import com.dark.mp3Utils.ProxySupplier;
import com.dark.site.Config;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.ning.http.client.AsyncHttpClient;

/**
 * Hello world!
 *
 */
public class App 
{
	private static final String P_TAG = "p";
	private AsyncHttpClient httpClient;
	private int delay = 1000;

    public static void main( String[] args ) throws IOException, SQLException, PropertyVetoException, InterruptedException
    {
    	Config config = new Config();
        JdbcPooledConnectionSource pooledConnectionSource = config.getJdbcPooledConnectionSource();
//        TableUtils.createTableIfNotExists(pooledConnectionSource, Mp3File.class);
//        TableUtils.createTableIfNotExists(pooledConnectionSource, Url.class);
        ProxySupplier proxySupplier = new ProxySupplier();
        (new Thread(proxySupplier)).start();
        List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < config.getMp3SearchThreadsCount(); i++) {
            Thread thread = new Thread(new com.dark.mp3search.client.search.Task(pooledConnectionSource));
            threads.add(thread);
            thread.start();
            Thread.sleep(30000);
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
//    	String html = "<p> <p>This is description with all available formatting. This text is<b> bold</b>, <i>italic</i>, <u>underlined</u>.</p> <p>This is left-aligned paragraph.</p> <p style=\"text-align: center;\">This is center-aligned paragraph.</p> <p style=\"text-align: right;\">This is right-aligned paragraph.</p> <p>This is <a href=\"http://sap.com\">external</a> link, this <a href=\"/content/sap-root/software-download.html\">one</a> is internal.</p> <p>This is bullet list:</p> <ul> <li>one</li> <li>two</li> <li>three</li> </ul> <p>This is numbered list:</p> <ol> <li>one</li> <li>two</li> <li>three</li> </ol> <p>&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; This text is indented.</p> </p>";
//    	//html = "<p>fgh <div> some text </div> <div>foo bar</div> </p>";
//    	String result = html;
//		Document doc = Jsoup.parse(html, "", Parser.xmlParser());
//		Elements childs = doc.children();
//		if (childs.size() == 1) {
//			Element el = childs.first();
//			if (el.tagName().equals(P_TAG)) {
//				result = childs.html();
//			}
//		}
//		System.out.println(result);
    	
//    	ExecutorService executor = Executors.newFixedThreadPool(1);
//    	AsyncHttpClientConfig config = (new AsyncHttpClientConfig.Builder()).setExecutorService(executor).setConnectionTimeoutInMs(5000).setAllowPoolingConnection(false).build();
//    	AsyncHttpClient client = new AsyncHttpClient(new GrizzlyAsyncHttpProvider(config), config);
//    	
//    	
//    	for (int i = 0; i < 100; i++) {
//    		final int requestId = i;
//	    	client.prepareGet("http://www.habrahabr.ru").execute(new AsyncCompletionHandler<Response>() {
//	
//				@Override
//				public Response onCompleted(Response response) throws Exception {
//					
//					System.out.println("---------------------");
//					System.out.println(requestId);
//					System.out.println(response.getStatusCode());
//					System.out.println(response.getHeaders());
////					System.out.println(response.getResponseBody());
//					
//					return response;
//				}
//				
//			    @Override
//			    public void onThrowable(Throwable t){
//			        t.printStackTrace();
//			    }				
//	    		
//	    	});
//    		try {
//				Thread.sleep(10);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//    	}
//    	
//    	while (true) {
//    		//System.out.println();
//    		try {
//				Thread.sleep(1000);
//			} catch (InterruptedException e) {
//				e.printStackTrace();
//			}
//    	}
    	
        
        
//    	ConnectionSource connectionSource = new JdbcConnectionSource("jdbc:mysql://localhost:3306/mp3", "root", "65536");
//    	Dao<Word, Long> wordsDao = DaoManager.createDao(connectionSource, Word.class);
//    	
//    	long idWordOffset = 0;
//    	while (true) {
//        	for (Word word : Word.getWords(wordsDao, idWordOffset, (long)10)) {
//        		if (word.getId() > idWordOffset) {
//        			idWordOffset = word.getId();
//        		}
//        		System.out.println(word.getId() + " " + word.getText());
//        	}
//    	}
    	
        
        
        DaoManager.createDao(pooledConnectionSource, Word.class);

        
    }
    
//    public void run() {
//        ExecutorService executor = Executors.newFixedThreadPool(20);
//        AsyncHttpClientConfig config = (new AsyncHttpClientConfig.Builder()).setExecutorService(executor).setConnectionTimeoutInMs(5000).setAllowPoolingConnection(false).build();
//        httpClient = new AsyncHttpClient(new GrizzlyAsyncHttpProvider(config), config);
//        new GoogleSite(httpClient, "adele 19 mp3 photoshop some other shit and crap", new GoogleSiteCompletionHandler() {
//            public void complete(List<String> links) {
//                System.out.println("Found: " + links.size());
//                processSourceLinks(links);
//            }
//        });
//        
//        while (true) {
//            System.out.println("still working");
//            System.out.println("Active downloads: " + Mp3ExtractTag.countActiveRequests.get());            
//            try {
//                Thread.sleep(1000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }        
//    }
//    
//    protected void processSourceLinks(List<String> links) {
//        for (String link : links) {
//            new Mp3LinksExtract(httpClient, link, new Mp3LinksExtractCompletionHandler() {
//                public void complete(List<String> links) {
//                    System.out.println("Found mp3 links: " + links.size());
//                    processMp3Links(links);
//                }
//            });
//            try {
//                Thread.sleep(delay);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        }
//    }
//    
//    protected void processMp3Links(List<String> links) {
//        for (String link : links) {
//            new Mp3ExtractTag(httpClient, link, new Mp3ExtractTagCompletionHandler() {
//                public void complete(File file, URI uri) {
//                    System.out.println("Done downloading file: " + uri.toString() + " file: " + file.getAbsolutePath() );
//                }
//            });
//        }
//        try {
//            Thread.sleep(delay);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//    }
    
}
