package com.dark.mp3search;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.dark.http.GoogleSiteCompletionHandler;
import com.dark.site.Google;
import com.ning.http.client.AsyncCompletionHandler;
import com.ning.http.client.AsyncHttpClient;
import com.ning.http.client.Request;
import com.ning.http.client.Response;

public class Mp3LinksExtract {
    private Mp3LinksExtractCompletionHandler handler;
    private List<String> links = new ArrayList<String>();
    Pattern mp3Regex = Pattern.compile("https?://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*\\.mp3", Pattern.CASE_INSENSITIVE);
    
    public Mp3LinksExtract(AsyncHttpClient httpClient, String url, Mp3LinksExtractCompletionHandler responseHandler) {
        int ttl = 3;
        handler = responseHandler;
        Request request = httpClient.prepareGet(url).build();
        sendRequest(httpClient, request, ttl);
    }
    
    protected void sendRequest(final AsyncHttpClient httpClient, final Request request, final int ttl) {
        if (ttl <= 0) {
            handler.complete(links);
            return;
        }
        try {
            System.out.println("Executing: " + request.getUrl());
            httpClient.executeRequest(request, new AsyncCompletionHandler<Response>() {
                @Override
                public Response onCompleted(Response response) throws Exception {
                    Matcher matcher = mp3Regex.matcher(response.getResponseBody());
                    while (matcher.find()) {
                        links.add(matcher.group());
                    }
                    handler.complete(links);
                    return response; 
                }
                
                @Override
                public void onThrowable(Throwable t) {
                    t.printStackTrace();
                    sendRequest(httpClient, request, ttl - 1);
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }    
}
