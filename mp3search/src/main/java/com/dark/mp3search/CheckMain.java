package com.dark.mp3search;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import com.dark.http.SimpleHttpClient;
import com.dark.mp3search.CheckMp3.Callback;
import com.dark.site.Config;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class CheckMain {
	private static Logger logger = Logger.getLogger(CheckMain.class);

    public static void main(String[] args)  {
    	Config config = new Config();
    	Connection connection = null;
    	try {
    	    JdbcPooledConnectionSource connectionSource = config.getJdbcPooledConnectionSource(); 
    	    
    		LinkedBlockingQueue<Runnable> queue = new LinkedBlockingQueue<Runnable>();
    		final ThreadPoolExecutor executorService = new ThreadPoolExecutor(config.getIntProp("mp3search.checkThreadsCount"), config.getIntProp("mp3search.checkThreadsCount"), 0L, TimeUnit.MILLISECONDS, queue);
 
    		
    		connection = DriverManager.getConnection(config.getStringProp("database.url") + "?user=" + config.getStringProp("database.user") + "&password=" + config.getStringProp("database.password"));
    		connection.setAutoCommit(false);
    	    PreparedStatement stat = connection.prepareStatement("SELECT * FROM `mp3` WHERE `status` > 0 AND `date_created` < ?", 
    	    		ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    	    stat.setTimestamp(1, new Timestamp(System.currentTimeMillis() - 24 * 60 * 60 * 1000));
	        stat.setFetchSize(Integer.MIN_VALUE);
	        ResultSet results = stat.executeQuery();
	        while (true) {
	            if (queue.size() < 1000 + config.getIntProp("mp3search.checkThreadsCount") * 20) {
	                boolean result = results.next();
	                if (!result) break;
	                CheckMp3 checkMp3 = new CheckMp3(results.getLong("id"), results.getString("url"), results.getString("source_url_hash"), connectionSource, new Callback() {
                        public void result(boolean isOk, CheckMp3 checkMp3) {
                            if (!isOk) {
                                checkMp3.recheck();
                                executorService.submit(checkMp3);
                            }
                        }
                    });
	                executorService.submit(checkMp3);
	            } else {
	                try {Thread.sleep(100);} catch (InterruptedException e) {logger.error(e, e);}
	            }
	            if (executorService.getActiveCount() == 0) break;
        	}
	        SimpleHttpClient.shutdownMonitor();
	        executorService.shutdown();
	        logger.info("Executor service shutdown");
		} catch (SQLException e) {
			logger.error(e, e);
		} finally {
			if (connection != null)
				try {
					connection.close();
				} catch (SQLException e) {
					logger.error(e, e);
				}
		}
    }
    
    

}
