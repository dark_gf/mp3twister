package com.dark.mp3search;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.UnsupportedAudioFileException;

import javazoom.spi.mpeg.sampled.file.MpegAudioFileReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Check;
import com.dark.mp3.model.Mp3CheckService;
import com.dark.mp3.model.Mp3Service;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class CheckMp3 implements Runnable {
    private static Logger logger = Logger.getLogger(CheckMp3.class);
    private final long recheckTime = 86400000l; //24 * 60 * 60 * 1000 
    
    private Long id;
    private String url;
    private String sourceUrlHash; 
    private JdbcPooledConnectionSource connectionSource;
    private Callback callback;
    private boolean recheck = false;
    
    public interface Callback {
        public void result(boolean isOk, CheckMp3 checkMp3);
    }
    
    public CheckMp3(Long id, String url, String sourceUrlHash, JdbcPooledConnectionSource connectionSource, Callback callback) {
        super();
        this.id = id;
        this.url = url;
        this.sourceUrlHash = sourceUrlHash;
        this.connectionSource = connectionSource;
        this.callback = callback;
    }

    public void recheck() {
        recheck = true;
    }
    
    private boolean isFileChecked(Mp3CheckService mp3FileCheckService) {
        Mp3Check mp3FileCheck = mp3FileCheckService.getLastCheck(id);
        if (mp3FileCheck != null && System.currentTimeMillis() - mp3FileCheck.getDate().getTime() < recheckTime) {
            logger.debug("mp3 file " + id + " already checked");
            return true;
        }
        return false;
    }
    
    private boolean isDomainUsed() {
        URI uri;
        try {
            uri = new URI(url);
            String domain = uri.getHost();
            while (!Mp3CheckService.getLock(domain, 250l)) {
                return true;
            }
        } catch (URISyntaxException e) {
            logger.warn(e, e);
        } 
        return false;
    }
    
    public void run() {
        
        try {
            Dao<Mp3Check, Long> mp3FileCheckDao = DaoManager.createDao(connectionSource, Mp3Check.class);
            Mp3CheckService mp3FileCheckService = new Mp3CheckService(mp3FileCheckDao);
            
            if (recheck) {
                if (isDomainUsed()) {
                    callback.result(false, this);
                    return;
                }
                if (isFileChecked(mp3FileCheckService)) {
                    callback.result(true, this);
                    return;
                }
            } else {
                if (isFileChecked(mp3FileCheckService)) {
                    callback.result(true, this);
                    return;
                }
                if (isDomainUsed()) {
                    callback.result(false, this);
                    return;
                }
            }

            logger.debug("checking "+ id + " file");
            

            Mp3 mp3 = parseMp3();
            
            if (mp3 != null) {
                mp3FileCheckService.inserNewCheck(id, 1);
            } else {
                mp3FileCheckService.inserNewCheck(id, 0);
                determineLiveOrDead(id, mp3FileCheckService);
            }
            
            
        } catch (SQLException e) {
            logger.error(e, e);
        }
        callback.result(true, this);
    }
    
    public Mp3 parseMp3() {
        Mp3 mp3 = null;
        RequestResponce response = null;
        InputStream input = null;
        try {
            SimpleHttpClient client = new SimpleHttpClient();
            response = client.doGetRequest(url, 60000);
            input = response.getResponse().getEntity().getContent();
            AudioFileFormat baseFileFormat = (new MpegAudioFileReader()).getAudioFileFormat(input);
            
            Map properties = baseFileFormat.properties();
            Long size = null;
            for (Header header : response.getResponse().getAllHeaders()) {
                if (header.getName().equalsIgnoreCase("Content-Length")) {
                    size = Long.valueOf(header.getValue());
                }
            }
            mp3 = Mp3.parse(baseFileFormat, size);
            if (mp3 != null) {
                mp3.setUrl(url);
                mp3.setSourceUrlHash(sourceUrlHash);
            }
        } catch (ClientProtocolException e) {
            logger.debug(e);
        } catch (IOException e) {
            logger.debug(e);
        } catch (UnsupportedAudioFileException e) {
            logger.debug(e);
        } catch (Exception e) {
            logger.error(e, e);
        } finally {
            if (response != null) response.getRequest().abort();
            IOUtils.closeQuietly(input);
            if (response != null)
                try {
                    response.getResponse().close();
                } catch (IOException e) {
                    logger.error(e);
                }
        } 
        return mp3;
    }
    
    public void determineLiveOrDead(Long id, Mp3CheckService mp3FileCheckService) {
        List<Mp3Check> mp3ChecksList = mp3FileCheckService.getChecks(id);
        if (mp3ChecksList.size() < 4) return;
        
        int good = 0;
        int bad = 0;
        for (Mp3Check mp3Check : mp3ChecksList) {
            if (mp3Check.getStatus() > 0) {
                good++;
            } else {
                bad++;
            }
        }
        if (bad > good) {
            //marking mp3 file as dead
            try {
                Dao<Mp3, String> dao = DaoManager.createDao(connectionSource, Mp3.class);
                Mp3Service mp3Service = new Mp3Service(dao);
                Mp3 mp3 = mp3Service.getById(id);
                mp3.setStatus(0);
                mp3Service.update(mp3);
                logger.debug("file " + mp3.getHash() + " url " + mp3.getUrl() + " is dead");
            } catch (SQLException e) {
                logger.error(e, e);
            }
            
            
            
        }
    }

}
