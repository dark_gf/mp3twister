package com.dark.mp3search;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.UnsupportedAudioFileException;

import javazoom.spi.mpeg.sampled.file.MpegAudioFileReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;
import com.dark.mp3.model.ConfigService;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.dark.mp3.model.Url;
import com.dark.mp3.model.Word;
import com.dark.mp3Utils.ProxySupplier;
import com.dark.mp3search.server.search.Mp3Result;
import com.dark.site.Google;
import com.dark.site.Utils;
import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class Task implements Runnable {
	private SimpleHttpClient client = new SimpleHttpClient();
	private Pattern mp3Regex = Pattern
			.compile(
					"https?://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*/[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*\\.mp3",
					Pattern.CASE_INSENSITIVE);
	private String serverUrl;

	static Logger logger = Logger.getLogger(Task.class);

	public Task(String serverUrl) {
		this.serverUrl = serverUrl;
	}

	public void run() {
		try {
			while (true) {
				ArrayList<Url> urls = new ArrayList<Url>();
				ArrayList<Mp3> mp3s = new ArrayList<Mp3>();

				String word = null;
				Integer id = null;
				try {
					RequestResponce response = client.doGetRequest(serverUrl
							+ "/?action=word", 30000);
					String text = SimpleHttpClient
							.httpResponseToString(response);
					if (text != null) {
						JSONObject jsonObject = new JSONObject(text);
						id = jsonObject.getInt("id");
						word = jsonObject.getString("word");
					}
				} catch (ClientProtocolException e) {
					logger.info(e);
				} catch (IOException e) {
					logger.info(e);
				} catch (IllegalStateException e) {
					logger.info(e);
				}

				if (word == null) {
					logger.warn("word does not recieved, sleeping 1 min");
					Thread.sleep(60000);
					continue;
				}
				logger.info("Got word: " + word);
				logger.info("Task id: " + id);

				// mp3skull
				Url url = new Url();
				url.setUrl("http://mp3skull.com/search.php?q="
						+ Utils.encode(word));
				urls.add(url);
				for (String mp3Link : getMp3LinksFromMp3skull(word)) {
					logger.info(mp3Link);
					Mp3 mp3 = checkMp3Link(mp3Link, url);
					if (mp3 != null) {
						mp3s.add(mp3);
					}
				}

				// mp3olimp
				url = new Url();
				url.setUrl("http://www.mp3olimp.net/search.php?query="
						+ Utils.encode(word));
				urls.add(url);
				for (String mp3Link : getMp3LinksFromMp3olimp(word)) {
					logger.info(mp3Link);
					Mp3 mp3 = checkMp3Link(mp3Link, url);
					if (mp3 != null) {
						mp3s.add(mp3);
					}
				}

				List<String> sourceLinks = getLinksFromGoogle(word);
				sourceLinks.addAll(getLinksFromGoogle(word + " mp3"));
				logger.info("Got " + sourceLinks.size() + " source links");
				for (String sourceLink : sourceLinks) {
					Url sourceUrlObj = new Url();
					sourceUrlObj.setUrl(sourceLink);
					urls.add(sourceUrlObj);
					List<String> mp3Links = getMp3Links(sourceLink);
					for (String mp3Link : mp3Links) {
						logger.info(mp3Link);
						Mp3 mp3 = checkMp3Link(mp3Link, sourceUrlObj);
						if (mp3 != null) {
							mp3s.add(mp3);
						}

					}
				}

				logger.info("Urls count: " + urls.size());
				logger.info("Mp3s found: " + mp3s.size());

				try {
					Mp3Result mp3Result = new Mp3Result(id, urls, mp3s);
					Gson gson = new Gson();
					gson.toJson(mp3Result);

					List<NameValuePair> nvps = new ArrayList<NameValuePair>();
					nvps.add(new BasicNameValuePair("data", gson
							.toJson(mp3Result)));

					RequestResponce response = client.doPostRequest(serverUrl
							+ "/?action=uploadMp3", nvps, 30000);
					String res = SimpleHttpClient
							.httpResponseToString(response);
					String ok = new String("OK");
					if (!res.equals(ok)) {
						logger.error("Result is NOT accepted by server: \""
								+ res + "\"");
					} else {
						logger.info("Result assepted by server.");
					}
				} catch (Exception e) {
					logger.error(e, e);
				}
			}
		} catch (Throwable t) {
			logger.error(t, t);
		}
	}

	private List<String> getLinksFromGoogle(String query) {
		List<String> result = new ArrayList<String>();

		int offset = 0;
		int ttl = 50;
		while (ttl > 0) {
			Google google = null;
			String url = "http://www.google.com/search?q="
					+ Utils.encode(query) + "&num=100&hl=en&prmd=imvnsu&start="
					+ Integer.toString(offset) + "&sa=N";
			try {
				RequestResponce response = client.doGetRequest(url, 30000,
						ProxySupplier.get());
				String html = SimpleHttpClient.httpResponseToString(response);
				if (html != null) {
					google = new Google(html);
					if (google.getResult() == Google.RESULT_OK) {
						result.addAll(google.getLinks());
						if (google.hasNextPage()) {
							offset += 100;
						}
					} else {
						google = null;
					}
				}
			} catch (ClientProtocolException e) {
				logger.info(e);
			} catch (IOException e) {
				logger.info(e);
			} catch (IllegalStateException e) {
				logger.info(e);
			} catch (IllegalArgumentException e) {
				logger.info(e);
			} catch (Exception e) {
				logger.error(e, e);
			}
			if (google != null && !google.hasNextPage())
				break;
			ttl--;
			if (ttl == 0) {
				logger.info("TTL 0, from google got " + result.size()
						+ " links");
			}
		}
		return result;
	}

	private List<String> getMp3Links(String url) {
		List<String> result = new ArrayList<String>();
		int ttl = 3;
		while (ttl > 0) {
			RequestResponce response = null;
			try {
				response = client.doGetRequest(url, 60000);
				String html = SimpleHttpClient.httpResponseToString(response);
				Matcher matcher = mp3Regex.matcher(html);
				while (matcher.find()) {
					result.add(matcher.group());
				}
				break;
			} catch (ClientProtocolException e) {
				logger.debug(e);
			} catch (IOException e) {
				logger.debug(e);
			} catch (IllegalArgumentException e) {
				logger.debug(e);
			} catch (IllegalStateException e) {
				logger.info(e);
			} catch (Exception e) {
				logger.error(e, e);
			}
			ttl--;
		}
		return result;
	}

	private List<String> getMp3LinksFromMp3olimp(String query) {
		HashSet<String> result = new HashSet<String>();
		int ttl = 30;
		while (ttl > 0) {
			// http://www.mp3olimp.net/search.php?query=linking+park
			String url = "http://www.mp3olimp.net/search.php?query="
					+ Utils.encode(query);
			try {
				RequestResponce response = client.doGetRequest(url, 30000,
						ProxySupplier.get());
				String html = SimpleHttpClient.httpResponseToString(response);
				if (html != null
						&& html.contains("Type in a song title, album or artist name")) {
					Matcher matcher = mp3Regex.matcher(html);
					while (matcher.find()) {
						String mp3Link = matcher.group();
						if (!mp3Link.toLowerCase().contains("mp3olimp.net"))
							result.add(matcher.group());
					}
					break;
				}
			} catch (ClientProtocolException e) {
				logger.info(e);
			} catch (IOException e) {
				logger.info(e);
			} catch (IllegalStateException e) {
				logger.info(e);
			} catch (IllegalArgumentException e) {
				logger.debug(e);
			} catch (Exception e) {
				logger.error(e, e);
			}			
			ttl--;
		}
		return new ArrayList<String>(result);
	}

	private List<String> getMp3LinksFromMp3skull(String query) {
		HashSet<String> result = new HashSet<String>();
		int ttl = 50;
		while (ttl > 0) {
			// http://www.mp3olimp.net/search.php?query=linking+park
			String url = "http://mp3skull.com/search.php?q="
					+ Utils.encode(query);
			try {
				RequestResponce response = client.doGetRequest(url, 30000,
						ProxySupplier.get());
				String html = SimpleHttpClient.httpResponseToString(response);
				if (html != null && html.contains("mp3skull.com")) {
					// Matcher matcher = mp3Regex.matcher(html);
					Document doc = Jsoup.parse(html, url);
					Elements els = doc.select("a");
					for (Element el : els) {
						String link = el.attr("abs:href");
						if (link.toLowerCase().endsWith(".mp3"))
							if (!link.toLowerCase().contains("mp3skull.com")) {
								result.add(link);
							}
					}
					break;
				}
			} catch (ClientProtocolException e) {
				logger.info(e);
			} catch (IOException e) {
				logger.info(e);
			} catch (IllegalStateException e) {
				logger.info(e);
			} catch (IllegalArgumentException e) {
				logger.debug(e);
			} catch (Exception e) {
				logger.error(e, e);
			}
			ttl--;
		}
		return new ArrayList<String>(result);
	}

	private Mp3 checkMp3Link(String url, Url sourceUrl) {
		int lastPos = url.lastIndexOf("/");
		if (lastPos > 0) {
			try {
				url = url.substring(0, lastPos + 1)
						+ URLEncoder.encode(
								URLDecoder.decode(url.substring(lastPos + 1),
										"UTF-8"), "UTF-8").replace("+", "%20");
			} catch (IllegalArgumentException e) {
				logger.debug(e, e);
			} catch (UnsupportedEncodingException e) {
				logger.debug(e);
			} catch (Exception e) {
				logger.error(e, e);
			}
		}

		RequestResponce response = null;
		InputStream input = null;
		Mp3 mp3 = null;
		try {
			response = client.doGetRequest(url, 60000);
			input = response.getResponse().getEntity().getContent();
			AudioFileFormat baseFileFormat = (new MpegAudioFileReader())
					.getAudioFileFormat(input);

			Map properties = baseFileFormat.properties();
			Long size = null;
			StringBuilder log = new StringBuilder();
			log.append("\n------------HEADERS-----------------\n");
			for (Header header : response.getResponse().getAllHeaders()) {
				if (header.getName().equalsIgnoreCase("Content-Length")) {
					size = Long.valueOf(header.getValue());
					log.append(header.getName() + " : " + header.getValue()
							+ "\n");
				}
			}
			log.append("-------------TAGS-------------------\n");
			for (Object key : properties.keySet()) {
				log.append(key + " : " + properties.get(key) + "\n");
			}
			log.append("-----------------------------------\n");
			mp3 = Mp3.parse(baseFileFormat, size);
			if (mp3 != null) {
				mp3.setUrl(url);
				mp3.setSourceUrlHash(sourceUrl.getMd5());
				log.append(mp3.getAuthor() + "\n");
				log.append(mp3.getAlbum() + "\n");
				log.append(mp3.getTitle() + "\n");
				log.append(mp3.getBitrate() + "\n");
				log.append(mp3.getFreq() + "\n");
				log.append(mp3.getSize() + "\n");
				log.append(mp3.getDuration() + "\n");
				log.append("-----------------------------------\n");
			} else {
				log.append("mp3 not valid \n");
				log.append("-----------------------------------\n");
			}
			logger.info(log.toString());
		} catch (ClientProtocolException e) {
			logger.info(e);
		} catch (IOException e) {
			logger.info(e);
		} catch (UnsupportedAudioFileException e) {
			logger.info(e);
		} catch (IllegalArgumentException e) {
			logger.info(e);
		} catch (IllegalStateException e) {
			logger.info(e);
		} catch (Exception e) {
			logger.error(e, e);
		} finally {
			if (response != null)
				response.getRequest().abort();
			IOUtils.closeQuietly(input);
			if (response != null)
				try {
					response.getResponse().close();
				} catch (IOException e) {
					logger.error(e, e);
				}
		}
		return mp3;
	}
}
