package com.dark.mp3search.server.search;

import java.util.ArrayList;

import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Url;

public class Mp3Result {
	private Integer id;
	private ArrayList<Url> urls;
	private ArrayList<Mp3> mp3s;
	
	public Mp3Result(Integer id, ArrayList<Url> urls, ArrayList<Mp3> mp3s) {
		super();
		this.id = id;
		this.urls = urls;
		this.mp3s = mp3s;
	}

	public Integer getId() {
		return id;
	}

	public ArrayList<Url> getUrls() {
		return urls;
	}

	public ArrayList<Mp3> getMp3s() {
		return mp3s;
	}
}
