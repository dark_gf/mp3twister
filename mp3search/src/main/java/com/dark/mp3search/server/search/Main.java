package com.dark.mp3search.server.search;

import com.dark.site.Config;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.dark.mp3search.server.search.Server;

public class Main {
	
	public static void main(String[] args) {
		Config config = new Config();
		JdbcPooledConnectionSource cs = config.getJdbcPooledConnectionSource();
		Controller.setPooledConnectionSource(cs);
		Server server = new Server(config.getIntProp("mp3search.server.search.port"));
		server.start();
	}

}
