package com.dark.mp3search.server.search;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.dark.mp3.model.ConfigService;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.dark.mp3.model.Url;
import com.dark.mp3.model.UrlService;
import com.dark.mp3.model.Word;
import com.dark.mp3.model.WorkQueue;
import com.dark.mp3.model.WorkQueueService;
import com.google.gson.Gson;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class Controller {
	private static Logger logger = Logger.getLogger(Controller.class);
	private static JdbcPooledConnectionSource pooledConnectionSource = null;

	private Dao<Word, Long> wordDao = null;
	private Dao<com.dark.mp3.model.Config, String> configDao = null;
	private Dao<WorkQueue, Long> workQueueDao = null;
	private Dao<Url, String> urlDao = null;
	private Dao<Mp3, String> mp3Dao = null;

	private WorkQueueService workQueueService = null;
	private UrlService urlService = null;
	private Mp3Service mp3Service = null;
	private static Long lastWordId = new Long(0);

	private SimpleDateFormat dateTimeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public static void setPooledConnectionSource(
			JdbcPooledConnectionSource connectionSource) {
		pooledConnectionSource = connectionSource;
	}

	public Controller() {
		try {
			configDao = DaoManager.createDao(pooledConnectionSource,
					com.dark.mp3.model.Config.class);
			wordDao = DaoManager.createDao(pooledConnectionSource, Word.class);
			workQueueDao = DaoManager.createDao(pooledConnectionSource,
					WorkQueue.class);
			urlDao = DaoManager.createDao(pooledConnectionSource, Url.class);
			mp3Dao = DaoManager.createDao(pooledConnectionSource, Mp3.class);

			workQueueService = new WorkQueueService(workQueueDao);
			urlService = new UrlService(urlDao);
			mp3Service = new Mp3Service(mp3Dao);
		} catch (SQLException e) {
			logger.error(e, e);
		}
	}

	public Object word() {
		JSONObject result = new JSONObject();
		synchronized (lastWordId) {
			String word = null;
        	WorkQueue workQueue = workQueueService.findStalledWork();
        	if (workQueue != null) {
        		//Found stalled work, need to push to worker one more time
        		logger.info("Stalled work found id: " + workQueue.getId());
        		String comment = workQueue.getComment();
        		Scanner scanner = new Scanner(comment);
        		while (scanner.hasNextLine()) {
    				word = scanner.nextLine();
        		}
        		scanner.close();
        		if (word == null) {
        			logger.error("Word is not exctracted from comment");
        			workQueue = null;
        			word = getNextWord();
        		} else {
        			workQueueService.addCommentAndUpdate(workQueue, "last update is " + dateTimeFormat.format(workQueue.getTime()) + ", work is stalled");        			
        		}
        	} else {
        		word = getNextWord();
        	}
			
			if (word != null) {
				if (workQueue == null) {
					workQueue = workQueueService.createNewWork(word);
				}
				result.put("id", workQueue.getId());
				result.put("word", word);
			}
		}

		return result;
	}

	private String getNextWord() {
		String result = null;
			ConfigService configService = new ConfigService(configDao);
			lastWordId = configService.getLastWordUsed();

			List<Word> words = Word.getWords(wordDao, lastWordId, 1);
			if (words.size() == 0) {
				// lastWordId = new Long(0);
				// words = Word.getWords(wordDao, lastWordId, 1);
			}
			for (Word word : words) {
				if (word.getId() > lastWordId) {
					lastWordId = word.getId();
				}
				result = word.getText();
			}

			configService.setLastWordUsed(lastWordId);
		return result;
	}

	@SuppressWarnings("unchecked")
	public String uploadMp3(String jsonText) {
		try {
			Gson gson = new Gson();
			Mp3Result mp3Result = gson.fromJson(jsonText, Mp3Result.class);
			Integer id = mp3Result.getId();
			ArrayList<Url> urls = mp3Result.getUrls();
			ArrayList<Mp3> mp3s = mp3Result.getMp3s();

			logger.info("Urls count: " + urls.size());
			logger.info("Mp3s found: " + mp3s.size());

			for (Mp3 mp3 : mp3s) {
				mp3.setDateUpdated(new Timestamp(System.currentTimeMillis()));
			}
			WorkQueue workQueue = workQueueService.findById(id);
			if (workQueue != null) {
				if (!workQueue.getStatus().equals(WorkQueue.STATUS_IN_WORK)) {
					return "Work with id " + id
							+ " does not have status in work";
				}
				urlService.insertBatch(urls);
				mp3Service.insertBatch(mp3s);
				workQueue.setComment(workQueue.getComment() + "\nStarted "
						+ dateTimeFormat.format(workQueue.getTime())
						+ " Mp3s found: " + mp3s.size() + " Urls count: "
						+ urls.size());
				workQueueService.setDone(workQueue);
			} else {
				return "Work with id " + id + " didn't found on server";
			}
		} catch (Exception e) {
			logger.error(e, e);
			return e.toString();
		}

		return "OK";
	}

}
