package com.dark.mp3search.server.search;

import io.netty.channel.ChannelInboundHandlerAdapter;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.http.DefaultFullHttpResponse;
import io.netty.handler.codec.http.DefaultHttpRequest;
import io.netty.handler.codec.http.FullHttpResponse;
import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.Attribute;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;
import io.netty.handler.codec.http.multipart.InterfaceHttpData;
import io.netty.handler.codec.http.multipart.InterfaceHttpData.HttpDataType;

import org.jboss.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import org.json.JSONObject;

import static io.netty.handler.codec.http.HttpHeaders.Names.*;
import static io.netty.handler.codec.http.HttpHeaders.Values;
import static io.netty.handler.codec.http.HttpResponseStatus.*;
import static io.netty.handler.codec.http.HttpVersion.*;

import com.dark.site.Utils;

public class Handler extends ChannelInboundHandlerAdapter {
	private static Logger logger = Logger.getLogger(Handler.class);

	private Controller controller = new Controller();

	@Override
	public void channelReadComplete(ChannelHandlerContext ctx) {
		ctx.flush();
	}

	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) {
		if (msg instanceof HttpRequest) {
			HttpRequest req = (HttpRequest) msg;

			if (HttpHeaders.is100ContinueExpected(req)) {
				ctx.write(new DefaultFullHttpResponse(HTTP_1_1, CONTINUE));
			}

			String uri = req.getUri();
			Map<String, String> params = new HashMap<String, String>();
			try {
				params = Utils.getParamsMap("http://localhost/" + uri);
			} catch (MalformedURLException ex) {
				logger.error(ex, ex);
			}
			String action = params.get("action");
			if (action == null) {
				action = "";
			}
			Object object = null;
			
			if (action.equals("word")) {
				object = controller.word();
			} else if (action.equals("uploadMp3")) {
				HttpPostRequestDecoder decoder = new HttpPostRequestDecoder(req);

				InterfaceHttpData data = decoder.getBodyHttpData("data");
				if (data.getHttpDataType() == HttpDataType.Attribute) {
					Attribute attribute = (Attribute) data;
					String value = null;
					try {
						value = attribute.getValue();
						object = controller.uploadMp3(value);
					} catch (IOException e) {
						logger.error(e, e);
						object = e.toString();
					}
				}

			} else {
				object = "Non valid request";
			}
			String content = object.toString();

			FullHttpResponse response;
			try {
				response = new DefaultFullHttpResponse(HTTP_1_1,
						OK, Unpooled.wrappedBuffer(content.getBytes("UTF-8")));
				response.headers().set(CONTENT_TYPE, "text/plain");
				response.headers().set(CONTENT_LENGTH,
						response.content().readableBytes());
	
				
				response.headers().set(CONNECTION, Values.CLOSE);
				ctx.write(response).addListener(ChannelFutureListener.CLOSE);
			} catch (UnsupportedEncodingException e) {
				logger.error(e,  e);
			}


		}
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		logger.error(cause, cause);
		ctx.close();
	}
}
