package com.dark.mp3search.client.search;

import java.beans.PropertyVetoException;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.dark.mp3.model.Word;
import com.dark.mp3Utils.ProxySupplier;
import com.dark.site.Config;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.ning.http.client.AsyncHttpClient;
import com.dark.mp3search.Task;

/**
 * Hello world!
 *
 */
public class Main 
{
	private static Logger logger = Logger.getLogger(Main.class);
    public static void main( String[] args ) throws IOException, SQLException, PropertyVetoException, InterruptedException
    {
    	Config config = new Config();
        ProxySupplier proxySupplier = new ProxySupplier();
        (new Thread(proxySupplier)).start();
        List<Thread> threads = new ArrayList<Thread>();
        for (int i = 0; i < config.getIntProp("mp3search.client.search.threadsCount"); i++) {
            Thread thread = new Thread(new Task(config.getStringProp("mp3search.client.search.serverUrl")));
            threads.add(thread);
            thread.start();
            Thread.sleep(5000);
        }
        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
            	logger.error(e, e);
            }
        }
    }    
}
