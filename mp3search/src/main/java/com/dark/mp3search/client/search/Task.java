package com.dark.mp3search.client.search;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.sound.sampled.AudioFileFormat;
import javax.sound.sampled.UnsupportedAudioFileException;

import javazoom.spi.mpeg.sampled.file.MpegAudioFileReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.Header;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import com.dark.http.SimpleHttpClient;
import com.dark.http.SimpleHttpClient.RequestResponce;
import com.dark.mp3.model.ConfigService;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.dark.mp3.model.Url;
import com.dark.mp3.model.Word;
import com.dark.mp3Utils.ProxySupplier;
import com.dark.site.Google;
import com.dark.site.Utils;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class Task implements Runnable {
    private SimpleHttpClient client = new SimpleHttpClient();
    private Pattern mp3Regex = Pattern.compile("https?://[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*/[-A-Za-z0-9+&@#/%?=~_()|!:,.;]*\\.mp3", Pattern.CASE_INSENSITIVE);
    JdbcPooledConnectionSource pooledConnectionSource;
    Dao<Url, String> urlDao = null;
    Dao<Word, Long> wordDao = null;
    private static Long lastWordId = new Long(0);
    
    static Logger logger = Logger.getLogger(Task.class);
    
    public Task(JdbcPooledConnectionSource pooledConnectionSource) {
    	this.pooledConnectionSource = pooledConnectionSource;
    }
    
    public void run() {
        try {
            urlDao = DaoManager.createDao(pooledConnectionSource, Url.class);
            wordDao = DaoManager.createDao(pooledConnectionSource, Word.class);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        while (true) {
        	String word = getNextWord();
        	if (word == null) {
        		logger.info("word does not recieved, exiting");
        		break;
        	}

            Url url = checkInsertOrUpdateUrl("http://mp3skull.com/search.php?q="+ Utils.encode(word));
            if (url != null) {
                for (String mp3Link : getMp3LinksFromMp3skull(word)) {
                    logger.info(mp3Link);
                    checkAndSaveMp3(mp3Link, url);
                }
            }
        	
        	
        	url = checkInsertOrUpdateUrl("http://www.mp3olimp.net/search.php?query=" + Utils.encode(word));
        	if (url != null) {
        	    for (String mp3Link : getMp3LinksFromMp3olimp(word)) {
                    logger.info(mp3Link);
                    checkAndSaveMp3(mp3Link, url);
                }
        	}
        	
        	
            List<String> sourceLinks = getLinksFromGoogle(word);
            sourceLinks.addAll(getLinksFromGoogle(word + " mp3"));
            logger.info("Got " + sourceLinks.size() + " source links");
            for (String sourceLink : sourceLinks) {
                Url sourceUrl = checkInsertOrUpdateUrl(sourceLink);
                if (sourceUrl != null) {
                    List<String> mp3Links = getMp3Links(sourceLink);
                    for (String mp3Link : mp3Links) {
                        logger.info(mp3Link);
                        checkAndSaveMp3(mp3Link, sourceUrl);
                    }
                }
            }
        }
        //System.out.println("done");
    }
    
    private Url checkInsertOrUpdateUrl(String url) {
        boolean result = true;
        Url urlObj = Url.get(url, urlDao);
        if (urlObj == null) {
            urlObj = new Url();
            urlObj.setUrl(url);
            try {
                urlDao.createOrUpdate(urlObj);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            if (System.currentTimeMillis() - urlObj.getTimestamp() >  1000 * 60 * 60 * 24) {
                urlObj.updateTimestamp();
                try {
                    urlDao.update(urlObj);
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            } else {
                result = false;
            }
        }
        if (result) return urlObj; 
        else return null;
    }
    
    private String getNextWord() {
        String result = null;
        synchronized (lastWordId) {
        	try {
				Dao<com.dark.mp3.model.Config, String> daoConfig = DaoManager.createDao(pooledConnectionSource, com.dark.mp3.model.Config.class);
				ConfigService configService = new ConfigService(daoConfig);
				lastWordId = configService.getLastWordUsed();
        	
	            List<Word> words = Word.getWords(wordDao, lastWordId, 1);
	            if (words.size() == 0) {
	//                lastWordId = new Long(0);
	//                words = Word.getWords(wordDao, lastWordId, 1);
	            }
	            for (Word word : words) {
	               if (word.getId() > lastWordId) {
	                   lastWordId = word.getId();
	               }
	               result = word.getText();
	            }
	            
	            configService.setLastWordUsed(lastWordId);
			} catch (SQLException e) {
				logger.error(e, e);
			}
        }
        return result;
    }
    
    private List<String> getLinksFromGoogle(String query) {
        List<String> result = new ArrayList<String>();
        
        int offset = 0;
        int ttl = 20;
        while (ttl > 0) {
            Google google = null;
            String url = "http://www.google.com/search?q=" + Utils.encode(query) + "&num=100&hl=en&prmd=imvnsu&start=" + Integer.toString(offset) + "&sa=N";
            try {
                RequestResponce response = client.doGetRequest(url, 30000, ProxySupplier.get());
                String html = SimpleHttpClient.httpResponseToString(response);
                if (html != null) {
                    google = new Google(html);
                    if (google.getResult() == Google.RESULT_OK) {
                        result.addAll(google.getLinks());
                        if (google.hasNextPage()) {
                            offset += 100;
                        }
                    } else {
                        
                        google = null;
                    }                    
                }
            } catch (ClientProtocolException e) {
                logger.info(e);
            } catch (IOException e) {
                logger.info(e);
            } catch (IllegalStateException e) {
                logger.info(e);
            }
            if (google != null && !google.hasNextPage()) break;
            ttl--;
        }
        return result;
    }
    
    private List<String> getMp3Links(String url) {
        List<String> result = new ArrayList<String>();
        int ttl = 3;
        while (ttl > 0) {
            RequestResponce response = null;
            try {
                response = client.doGetRequest(url, 60000);
                String html = SimpleHttpClient.httpResponseToString(response);
                Matcher matcher = mp3Regex.matcher(html);
                while (matcher.find()) {
                    result.add(matcher.group());
                }
                break;
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ttl--;
        }
        return result;
    }
    
    private List<String> getMp3LinksFromMp3olimp(String query) {
        HashSet<String> result = new HashSet<String>();
        int ttl = 30;
        while (ttl > 0) {
            //http://www.mp3olimp.net/search.php?query=linking+park
            String url = "http://www.mp3olimp.net/search.php?query=" + Utils.encode(query);
            try {
                RequestResponce response = client.doGetRequest(url, 30000, ProxySupplier.get());
                String html = SimpleHttpClient.httpResponseToString(response);
                if (html != null && html.contains("Type in a song title, album or artist name")) {
                    Matcher matcher = mp3Regex.matcher(html);
                    while (matcher.find()) {
                        String mp3Link = matcher.group();
                        if (!mp3Link.toLowerCase().contains("mp3olimp.net")) result.add(matcher.group());
                    }
                    break;
                }
            } catch (ClientProtocolException e) {
                logger.info(e);
            } catch (IOException e) {
                logger.info(e);
            } catch (IllegalStateException e) {
                logger.info(e);
            }
            ttl--;
        }
        return new ArrayList<String>(result);
    }
    
    private List<String> getMp3LinksFromMp3skull(String query) {
        HashSet<String> result = new HashSet<String>();
        int ttl = 30;
        while (ttl > 0) {
            //http://www.mp3olimp.net/search.php?query=linking+park
            String url = "http://mp3skull.com/search.php?q="+ Utils.encode(query);
            try {
                RequestResponce response = client.doGetRequest(url, 30000, ProxySupplier.get());
                String html = SimpleHttpClient.httpResponseToString(response);
                if (html != null && html.contains("mp3skull.com")) {
                    //Matcher matcher = mp3Regex.matcher(html);
                    Document doc = Jsoup.parse(html, url);
                    Elements els = doc.select("a");
                    for (Element el : els) {
                        String link = el.attr("abs:href");
                        if (link.toLowerCase().endsWith(".mp3")) if (!link.toLowerCase().contains("mp3skull.com")){
                            result.add(link);
                        }
                    }
                     break;
                }
            } catch (ClientProtocolException e) {
                logger.info(e);
            } catch (IOException e) {
                logger.info(e);
            } catch (IllegalStateException e) {
                logger.info(e);
            }
            ttl--;
        }
        return new ArrayList<String>(result);
    }    
    
    private void checkAndSaveMp3(String url, Url sourceUrl) {
        int lastPos = url.lastIndexOf("/");
        if (lastPos > 0) {
            try {
                url = url.substring(0, lastPos + 1) + URLEncoder.encode(URLDecoder.decode(url.substring(lastPos + 1), "UTF-8"), "UTF-8").replace("+", "%20");
            } catch (UnsupportedEncodingException e) {
                logger.error(e, e);
            }
        }
        
        RequestResponce response = null;
        InputStream input = null;
        Mp3 mp3 = null;
        try {
            response = client.doGetRequest(url, 60000);
            input = response.getResponse().getEntity().getContent();
            AudioFileFormat baseFileFormat = (new MpegAudioFileReader()).getAudioFileFormat(input);
            
            Map properties = baseFileFormat.properties();
            Long size = null;
            StringBuilder log = new StringBuilder();
            log.append("\n------------HEADERS-----------------\n");
            for (Header header : response.getResponse().getAllHeaders()) {
                if (header.getName().equalsIgnoreCase("Content-Length")) {
                    size = Long.valueOf(header.getValue());
                    log.append(header.getName() + " : " + header.getValue() + "\n");
                }
            }
            log.append("-------------TAGS-------------------\n");
            for (Object key: properties.keySet()) {
                log.append(key + " : " + properties.get(key) + "\n");
            }
            log.append("-----------------------------------\n");
            mp3 = Mp3.parse(baseFileFormat, size);
            if (mp3 != null) {
                mp3.setUrl(url);
                mp3.setSourceUrlHash(sourceUrl.getMd5());
                log.append(mp3.getAuthor() + "\n");
                log.append(mp3.getAlbum() + "\n");
                log.append(mp3.getTitle() + "\n");
                log.append(mp3.getBitrate() + "\n");
                log.append(mp3.getFreq() + "\n");
                log.append(mp3.getSize() + "\n");
                log.append(mp3.getDuration() + "\n");
                log.append("-----------------------------------\n");
            } else {
                log.append("mp3 not valid \n");
                log.append("-----------------------------------\n");
            }
            logger.info(log.toString());
        } catch (ClientProtocolException e) {
            logger.info(e);
        } catch (IOException e) {
            logger.info(e);
        } catch (UnsupportedAudioFileException e) {
            logger.info(e);
        } catch (Exception e) {
            logger.error(e, e);
            //http://promodj.com/download/4849528/Linkin Park - Final Masquerade  [www.kurmet-records.kz] (promodj.com).mp3
            //http://promodj.com/download/4849528/Linkin%20Park%20-%20Final%20Masquerade%20%20[www.kurmet-records.kz]%20(promodj.com).mp3
            //http://promodj.com/download/4849528/Linkin%20Park%20-%20Final%20Masquerade%20%20[www.kurmet-records.kz]%20(promodj.com).mp3
        } finally {
        	if (response != null) response.getRequest().abort();
            IOUtils.closeQuietly(input);
            if (response != null)
                try {
                    response.getResponse().close();
                } catch (IOException e) {
                    logger.error(e);
                }
        }
        if (mp3 != null) {
            Dao<Mp3, String> mp3Dao = null;
            try {
                mp3Dao = DaoManager.createDao(pooledConnectionSource, Mp3.class);
            } catch (SQLException e) {
                logger.error(e, e);
            }
            if (mp3Dao != null) {
                Mp3Service mp3FileService = new Mp3Service(mp3Dao);
                if (mp3FileService.getByUrl(mp3.getUrl()) == null) {
                    try {
                        mp3Dao.createIfNotExists(mp3);
                    } catch (SQLException e) {
                        logger.error(e, e);
                    }
                }
            }
        }
        
    }
    
}
