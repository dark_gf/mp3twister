package com.dark.mp3indexer;

import org.apache.log4j.Logger;

import com.dark.site.Config;

public class Main {
    static Logger logger = Logger.getLogger(Main.class);
    
    public static void main(String[] args) {
        Config config = new Config();
        Indexer indexer = new Indexer(config.getSolrInstance(), config.getJdbcPooledConnectionSource());
        indexer.run();
    }

}
