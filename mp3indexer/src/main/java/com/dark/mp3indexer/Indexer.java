package com.dark.mp3indexer;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrServer;
import org.apache.solr.common.SolrInputDocument;

import com.dark.mp3.model.Config;
import com.dark.mp3.model.ConfigService;
import com.dark.mp3.model.Mp3;
import com.dark.mp3.model.Mp3Service;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;

public class Indexer {
    private final static Long CHUNK_SIZE = 1000l; 
    
    static Logger logger = Logger.getLogger(Indexer.class);
    
    private HttpSolrServer solrServer;
    private JdbcPooledConnectionSource pooledSource;
    
    public Indexer(HttpSolrServer solrServer, JdbcPooledConnectionSource pooledSource) {
        this.solrServer = solrServer;
        this.pooledSource = pooledSource;
    }
    
    public void run() {
        Dao<Mp3, String> mp3Dao = null;
        Dao<Config, String> configDao = null;
        try {
            mp3Dao = DaoManager.createDao(pooledSource, Mp3.class);
            configDao = DaoManager.createDao(pooledSource, Config.class);
        } catch (SQLException e) {
            logger.error(e);
        }
        ConfigService configService = new ConfigService(configDao);
        Mp3Service mp3FileService = new Mp3Service(mp3Dao);
        
        Timestamp lastUsed = configService.getLastDateIndexerUsed();
        
        //configService.setLastDateIndexerUsed(new Timestamp(System.currentTimeMillis() / 1000));
        while (true) {
            List<Mp3> mp3Files = mp3FileService.getFromDateUpdated(lastUsed, CHUNK_SIZE);
            logger.info("Got " + mp3Files.size() + " mp3 files for push to solr server");
            
            
            for (Mp3 mp3File : mp3Files) {
                if (mp3File.getStatus() > 0) {
                    String urlPath = mp3File.getUrl().replace("/", " ");
                    if (urlPath.toLowerCase().startsWith("http:")) {
                        urlPath = urlPath.substring(5);
                    }
                    try {
                        urlPath = URLDecoder.decode(urlPath, "UTF-8");
                    } catch (UnsupportedEncodingException e) {
                        logger.error(e);
                        urlPath = "";
                    }
                    if (urlPath.toLowerCase().endsWith(".mp3")) {
                        urlPath = urlPath.substring(0, urlPath.length() - 4);
                    }
                    
                    SolrInputDocument doc = new SolrInputDocument();
                    doc.addField("id", mp3File.getHash());
                    doc.addField("name", mp3File.getName());
                    doc.addField("author", mp3File.getAuthor());
                    doc.addField("album", mp3File.getAlbum());
                    doc.addField("title", mp3File.getTitle());
                    doc.addField("url_path", urlPath);
                    StringBuilder allText = new StringBuilder();
                    if (mp3File.getAuthor() != null) {
                        allText.append(mp3File.getAuthor() + " ");
                    }
                    if (mp3File.getAlbum() != null) {
                        allText.append(mp3File.getAlbum() + " ");
                    }
                    if (mp3File.getTitle() != null) {
                        allText.append(mp3File.getTitle() + " ");
                    }
                    allText.append(urlPath);
                    doc.addField("all_text", allText.toString());
                    try {
                        solrServer.add(doc);
                    } catch (SolrServerException e) {
                        logger.error(e, e);
                    } catch (IOException e) {
                        logger.error(e, e);
                    }
                } else {
                    //removing file from index
                    try {
                        solrServer.deleteById(mp3File.getHash());
                    } catch (SolrServerException e) {
                        logger.error(e, e);
                    } catch (IOException e) {
                        logger.error(e, e);
                    }
                }
                lastUsed = mp3File.getDateUpdated();
            }
            try {
                solrServer.commit();
            } catch (SolrServerException e) {
                logger.error(e, e);
            } catch (IOException e) {
                logger.error(e, e);
            }
            configService.setLastDateIndexerUsed(lastUsed);
            
            if (mp3Files.size() < CHUNK_SIZE) {
                try {Thread.sleep(60000);} catch (InterruptedException e) {logger.warn(e);}
            }
        }
    }
    
    
}
